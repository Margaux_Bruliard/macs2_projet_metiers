# -*- coding: utf-8 -*-
# Created on [2016-10-13 Thu] by Sylvain Girard (girard@phimeca.com)
"""Physical model of the balistic trajectory of petanque throws.
"""

#§
from scipy.constants import g as gravity
import numpy as np

def optimal_velocity(distance, latitude):
    """Compute optimal velocity.

    distance -- Float, in metre.

    latitude -- Float, in degree.
    """

    flight_time = np.sqrt(2 * distance / gravity *
                          np.tan(np.deg2rad(latitude)))
    return np.sqrt((distance / flight_time)**2 +
                   (gravity * flight_time / 2)**2)

def throw(latitude, longitude, velocity):
    """Compute arrival coordinates and flight time."""
    latitude = np.deg2rad(latitude)
    longitude = np.deg2rad(longitude)
    velocity_vertical = np.sin(latitude) * velocity
    velocity_horizontal = np.cos(latitude) * velocity
    flight_time = 2 * velocity_vertical / gravity
    distance = velocity_horizontal * flight_time
    return (np.sin(longitude) * distance, np.cos(longitude) * distance,
            flight_time)

hit_point = throw # alias.

def reverse_throw(xx, yy, flight_time):
    """Get parameters from hit point."""
    longitude = np.arctan2(yy , xx)
    distance = np.sqrt(xx**2 + yy**2)
    velocity_horizontal = distance / flight_time
    velocity_vertical = flight_time * gravity / 2
    velocity = np.sqrt(velocity_horizontal**2 + velocity_vertical**2)
    latitude = np.arccos(velocity_horizontal / velocity)
    return np.rad2deg(latitude), 90. - np.rad2deg(longitude), velocity

def compute_score(distance, xx, yy):
    """Compute distance to goal."""
    return np.sqrt(xx**2 + (distance - yy)**2)

#§ Graphics
from matplotlib import pyplot as plt
from matplotlib.patches import Rectangle
from scipy.stats.mstats import mquantiles

def hist_score(data, n_bin, color, probability=None):
    """Histogram of scores."""
    if probability is None:
        probability = [.05, .25, .75, .95]
    plt.hist(data, bins=n_bin, color=color, normed=True)
    average = data.mean()
    plt.axvline(average, lw=2.5, color="k")
    for quantile in mquantiles(data, probability):
        plt.axvline(quantile, lw=2.5, color="SlateGray")
    plt.annotate("Moyenne : %.2g" % average, (.75, .75),
                 xycoords="axes fraction", size=14)

def draw_field(distance):
    plt.plot([-2, 2], [0, 0], "k", lw=1)
    plt.plot(0, 0, "ok", ms=30)
    plt.plot(0, distance, marker="o", mfc="Wheat", ms=10, mew=2, mec="k")
    plt.gca().add_patch(Rectangle((-2,-1), 4, 15, ec="k", fc="none", lw=2))
    plt.axis("equal")
    plt.ylim([-1.2, 15.2])
    plt.legend()

def plot_throw(xx, yy, color, label=None, **kwargs):
    plt.plot(xx, yy, color=color, label=label, **kwargs)

def plot_list_throw(list_xx, list_yy, list_color, list_label, distance,
                    **kwargs):
    for args in zip(list_xx, list_yy, list_color, list_label):
        plot_throw(*args, **kwargs)
    draw_field(distance)

#§
# Local Variables:
# tmux-temp-file: "/cluster/home/girard/.tmp/tmux_buffer"
# tmux-repl-window: "form"
# End:
