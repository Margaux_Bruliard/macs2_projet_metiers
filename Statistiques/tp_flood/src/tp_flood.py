"""
BRULIARD Margaux - SMITI Samah
MACS 2

Statistiques Descriptives - TP Flood
"""

import numpy as np
from matplotlib import pyplot as plt
from scipy import stats # pour regression lineaire
import csv 

#==============================================================================
#   DEFINITIONS DES FONCTIONS USERS

def chargerDonnees ():
	
	with open('Donnees_debit_trans.csv', newline='') as csvfile:
		spamreader = csv.reader(csvfile, delimiter=';')
		
	
		anneeTohauteur = []
		hauteur =  []
		hauteurTodebit = []
	
		for line in spamreader:
			# print(line)
			if not '' == line[2]:
				#la hauteur est donnee
				anneeTohauteur.append( [int(line[0]), float(line[2]) ] )
				hauteur.append(float(line[2]))
				
		
				# hauteur et debit
				hauteurTodebit.append([float(line[2]), float(line[1])])
		
		return hauteur, anneeTohauteur, hauteurTodebit


    
    
    
    
#==============================================================================

#==============================================================================
# QUESTION 1
    #NUAGE DE POINTS SUR ANNEE/HAUTEUR

#AtoH, HtoDeb =
H, AH, HDeb = chargerDonnees()


#nuage de points sur indépendance de H
anneeFromHauteur = []
for i in range(len(AH)):
	anneeFromHauteur.append(AH[i][0])

plt.scatter(anneeFromHauteur, H)
plt.title ("Hauteur de l'eau de la riviere selon les années")
plt.xlabel("Année")
plt.ylabel("Hauteur de l'eau (m)")
plt.show()



    #HISTOGRAMME DE LA LOI DE H
print("Histogramme des H")
plt.hist(H, normed=1)
plt.title("histogramme de la répartition des valeurs de H")
plt.show()


#==============================================================================


#==============================================================================
# QUESTION 2:
    #NUAGE DE POINTS HAUTEUR/DEBIT

print("Creation d'un nuage de points pour montrer la dependance entre la hauteur de l'eau et le débit")
hauteur_eau_q2 = []
debit_eau_q2  = []
for i in range(len(HDeb)):
	hauteur_eau_q2.append(HDeb[i][0])
	debit_eau_q2.append(HDeb[i][1])

plt.scatter(debit_eau_q2, hauteur_eau_q2)

PostulatLogHi = np.log(debit_eau_q2);

coef_puissance = 0.285
print("on choisit arbitrairement coef_puissance", coef_puissance)
PostulatPuissanceHi = []
PostulatProduitHi = []
for i in range(len(debit_eau_q2)):
	PostulatPuissanceHi.append(debit_eau_q2[i]**coef_puissance)

plt.plot (debit_eau_q2, PostulatLogHi, 'r');
plt.plot (debit_eau_q2, PostulatPuissanceHi, 'm')








#==============================================================================
