"""
BRULIARD Margaux
MACS 2

Statistiques Descriptives - TP Flood - PARTIE 2
"""

import numpy as np
from matplotlib import pyplot as plt
from scipy import stats # pour regression lineaire
import scipy.special as sps
import csv
import math

#==============================================================================
#   DEFINITIONS DES FONCTIONS USERS

def chargerDonnees ():
	
	with open('Donnees_debit_trans.csv', newline='') as csvfile:
		spamreader = csv.reader(csvfile, delimiter=';')
		
	
		anneeToDebit = []
		debit =  []
	
		for line in spamreader:
			#on recupere le debit pour chaque ligne
			debit.append(float(line[1]))
			anneeToDebit.append([int(line[0]), float(line[1])])
			
		
		return debit, anneeToDebit


def determinationQuantile (val_quantile, echantillon):
    nb_total_elem_echantillon = len(echantillon);
    indice_quantile = val_quantile*nb_total_elem_echantillon
    if (indice_quantile - int(indice_quantile) == 0):
        #cela signifie que indice_quantile est un entier
        return echantillon[indice_quantile]
    else:
        return echantillon[int(indice_quantile) + 1]
    
def densiteLoiGamma(x, k, theta):
	xpowkminus1 = x**(k-1)
	valGamma = sps.gamma(k);
	thetapowk = theta**k
	
	res= xpowkminus1*np.exp(-x/theta) /(valGamma*thetapowk);
	return res;
    
    
    
#==============================================================================

#==============================================================================
#CHARGEMENT DES DONNEES
debit, anneeToDebit = chargerDonnees()


#nuage de points debits
annee = []
for i in range(len(anneeToDebit)):
	annee.append(anneeToDebit[i][0])

plt.scatter(annee, debit, color='g')
plt.title("Répartition du débit selon les années")
plt.xlabel("année")
plt.ylabel("débit en $m^3.s^{-1}$")
plt.show()


#Histogramme de debit
print("Histogramme des débits")
plt.hist(debit, normed=1, facecolor='g')
plt.xlabel("débit")
plt.title("histogramme de la répartition des valeurs des debits")
plt.show()


# calcul de la moyenne empirique de l'échantillon
debit_moyenne_empirique = np.mean(debit)
debit_sigma_carree_sans_biais = np.var(debit)
#somme= 0;
#for i in range(len(debit)):
#	somme = somme +debit[i]**2

#calcul de l'estimation de la variance de l'echantillon
#debit_sigma_carree_sans_biais = 1/(len(debit))*(somme + len(debit)*debit_moyenne_empirique**2)
print("\nMoyenne empirique de debit: ", debit_moyenne_empirique, "\nestimation de la variance: ", debit_sigma_carree_sans_biais)

# on en deduit k et theta les parametres de la loi Gamma
theta = debit_sigma_carree_sans_biais/debit_moyenne_empirique
k = debit_moyenne_empirique/theta
print("\nPARAMETRES DE LA LOI GAMMA\ntheta = ", theta, "\nk= ", k, "\n")


#on trace a nouveau l'histogramme avec la densite associée
n , bins, patches = plt.hist(debit, normed=True, facecolor='g')
xvalues = np.linspace(bins[0], bins[-1], num=100)
#yvalues = densiteLoiGamma(xvalues, k, theta )
yvalues = []
for i in range(len(xvalues)):
	yvalues.append(densiteLoiGamma(xvalues[i], k, theta))

plt.plot(xvalues, yvalues, label='densite de la loi Gamma', color='y')
plt.xlabel("débit")
plt.title ("Histogramme des débits avec densité de loi associée")
plt.show()










#==============================================================================
