"""
BRULIARD Margaux - SMITI Samah
MACS 2

Statistiques Descriptives - TP Flood
"""

import numpy as np
from matplotlib import pyplot as plt
from scipy import stats # pour regression lineaire
import csv 

#==============================================================================
#   DEFINITIONS DES FONCTIONS USERS

def chargerDonnees ():
	
	with open('Donnees_debit_trans.csv', newline='') as csvfile:
		spamreader = csv.reader(csvfile, delimiter=';')
		
	
		anneeTohauteur = []
		hauteur =  []
		hauteurTodebit = []
		debit = []
	
		for line in spamreader:
			# print(line)
			if not '' == line[2]:
				#la hauteur est donnee
				anneeTohauteur.append( [int(line[0]), float(line[2]) ] )
				hauteur.append(float(line[2]))
				
		
				# hauteur et debit
				hauteurTodebit.append([float(line[2]), float(line[1])])
			debit.append(float(line[1]))
		return hauteur, debit, anneeTohauteur, hauteurTodebit


def determinationQuantile (val_quantile, echantillon):
    nb_total_elem_echantillon = len(echantillon);
    indice_quantile = val_quantile*nb_total_elem_echantillon
    if (indice_quantile - int(indice_quantile) == 0):
        #cela signifie que indice_quantile est un entier
        return echantillon[indice_quantile]
    else:
        return echantillon[int(indice_quantile) + 1]
    
    
    
    
#==============================================================================

#==============================================================================
# QUESTION 1
    #NUAGE DE POINTS SUR ANNEE/HAUTEUR

#AtoH, HtoDeb =
H, D, AH, HDeb = chargerDonnees()


#nuage de points sur indépendance de H
anneeFromHauteur = []
for i in range(len(AH)):
	anneeFromHauteur.append(AH[i][0])

plt.scatter(anneeFromHauteur, H)
plt.title ("Hauteur de l'eau de la riviere selon les années")
plt.xlabel("Année")
plt.ylabel("Hauteur de l'eau (m)")
plt.show()



    #HISTOGRAMME DE LA LOI DE H
print("Histogramme des H")
plt.hist(H, normed=1)
plt.title("histogramme de la répartition des valeurs de H")
plt.show()



    #CALCUL DES PARAMETRES DE LA LOI DE H
#on suppose une loi normale en se basant sur l'histogramme
print ("\n")
H_moyenne_empirique = 1/len(H)*sum(H);

somme= 0;
for i in range(len(H)):
	somme = somme +H[i]**2
	

H_sigma_carree_sans_biais = 1/(len(H))*(somme + len(H)*H_moyenne_empirique**2)
print("H suit une loi normale de parametre (", H_moyenne_empirique, ",", H_sigma_carree_sans_biais, ")")


    #CALCUL DU QUANTILE 0.9 DE L'ECHANTILLON H
print("\nCALCUL DU QUANTILE 0.9 DE H:")
H.sort()
val_quantile = determinationQuantile(0.9, H)
print(val_quantile, "\n")

#==============================================================================


#==============================================================================
# QUESTION 2:
    #NUAGE DE POINTS HAUTEUR/DEBIT

print("Creation d'un nuage de points pour montrer la dependance entre la hauteur de l'eau et le débit")
hauteur_eau_q2 = []
debit_eau_q2  = []
for i in range(len(HDeb)):
	hauteur_eau_q2.append(HDeb[i][0])
	debit_eau_q2.append(HDeb[i][1])

plt.scatter(debit_eau_q2, hauteur_eau_q2, label=" ")
plt.title("Hauteur de l'eau de la riviere en fonction de son débit")
plt.ylabel("hauteur de l'eau (en m)")
plt.xlabel("débit de l'eau (en m3/s)")


PostulatLogHi = np.log(debit_eau_q2);

coef_puissance = 0.285
print("on choisit arbitrairement coef_puissance", coef_puissance)
PostulatPuissanceHi = []
PostulatProduitHi = []
for i in range(len(debit_eau_q2)):
	PostulatPuissanceHi.append(debit_eau_q2[i]**coef_puissance)

# plt.plot (debit_eau_q2, PostulatLogHi, label="postulat $H_i = \log(Q_i)$");
# plt.plot (debit_eau_q2, PostulatPuissanceHi, label="postulat $H_i = (Q_i)^a$")



# Regression Lineaire
R_i = np.log(debit_eau_q2)
S_i = np.log(hauteur_eau_q2)
alpha, gamma, r_value, p_value, std_err = stats.linregress(R_i, S_i)

beta = np.exp(gamma)
print("\nREGRESSION LINEAIRE")
print("Constantes de la regression linéaire: alpha=", alpha , " gamma=", gamma)
print("Constantes de la formule: alpha=", alpha, " beta=", beta)

# calcul de l'estimation de H 
H_estimationReg =[]
for i in range(len(debit_eau_q2)):
	H_estimationReg.append(beta*debit_eau_q2[i]**alpha) 	
	
plt.plot(debit_eau_q2, H_estimationReg, label="hauteur de l'eau estimée par regression linéaire", color='y')
plt.show()



	# CALCUL DE L'INCERTITUDE ENTRE H ET H_ESTIMEE

# on recupere le débit correspondant au quantile 0.9
D.sort()
debit_quantile = determinationQuantile(0.9, D)
print("\nCALCUL DU H ESTIME ISSU DU DEBIT:valeur du débit pour le quantile 0.9: ", debit_quantile)
#on utilise la regression lineaire pour trouver H_estime
H_estime_quantile = beta*debit_quantile**alpha

print("Le H estimé pour le quantile 0.9 est ", H_estime_quantile)
#==============================================================================
