# -*- coding: utf-8 -*-
"""
Created on Sun May 20 12:45:08 2018

@author: Margaux Bruliard

MACS 2 - PROJET METIER: STATISTIQUES
Exercice de la balance romaine
"""



import numpy as np
from matplotlib import pyplot as plt
import numpy.random as npr


def incertitudeBalanceRomaine (y, m0, sigma, N):
	"""
	On cherche a determiner l erreur de mesure de la balance romaine

	soit Y = M X le modele de la balance
		@Y represente la masse que l on souhaite calculer
		@M represente la masse du peson: M = m0 + mu, mu suit une loi normale (0, sigma)
		@X represente la longueur (graduations de la balance)
	"""
	
	# on genere un N echantillon de masse
	M = npr.normal (m0, sigma, N)

	# comme Y = MX -> X = y/M
	X = y/M
	
	# on pose l'erreur de X comme 
	Xincert = np.round (X)
	

	# on en deduit alors l erreur sur Y qui decoule de l erreur sur X
	Yincert = m0*Xincert
	
	
	# l erreur est donc 
	err = Yincert - y;
	
	
	return err;


"""
FONCTION PRINCIPALE
"""

y = [750, 1050, 1520]
m0 = 500
sigma = [40., 5., 45.]
N = 100


for i in [0,1,2]:
	
	err = incertitudeBalanceRomaine(y[i], m0, sigma[i], N);
	

	plt.hist(err, normed=1);
	if (i ==0):
		plt.title("Histogramme de l'incertitude pour y=750 g")
	if (i==1):
		plt.title("Histogramme de l'incertitude pour y=1000 g")
	if (i==2):
		plt.title("Histogramme de l'incertitude pour y=1500 g")
	plt.show();
	
	#print("Loi normale de moyenne", np.mean(err))
	#print ("L'erreur maximale de mesure est ", max(abs(err)), " grammes\n");




