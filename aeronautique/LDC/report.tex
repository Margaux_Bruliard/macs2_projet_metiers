\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{amsmath, amsfonts, amsthm}
\usepackage{color}
\usepackage{xcolor}


\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height

\author{Margaux BRULIARD \\ MACS 2 -  Sup'Galilée}
\title{
		\bfseries
		\horrule{1pt}\\
		\huge LDC Scheme Project \\ \large Aeronautic course
		\horrule{1.5pt}
		}
\date{\today}


\begin{document}
	\maketitle
	
	%-----------------------------------PARTIE ADVECTION DIFFUSION----------------------------%
	\section{Advection-Diffusion scheme}
	
	We would like to study this \textbf{advection-diffusion equation}:
	
	\begin{equation}
		\label{advdiff}
		\left\{
			\begin{array}{llll}
				\partial_t u + a \partial_x u - \nu^2 \partial_{xx}u = g \\
				\Omega = ] -3;3 [ \\
				a = 1 \\
				\nu = 0.01 \\
				u_0(x) = u(x, 0) = x^2	\\
				g (x, t) = 2t + 2x + 2
			\end{array}
		\right.
	\end{equation}
	
	\subsection{Proof that RK2 is in a 2nd order scheme in time}
	
		We consider $u(t^n, x_i) = u_i^n$, where $t^n$ and $x_i$ are discretization in time and space. 
		
		As we are looking for time order, we will just study $u^n = u(t^n, x), \quad \forall x \in \{1, ..., Nx\}$.
		
		According to \underline{the third Taylor expansion}, 
		
		\begin{align*}
			u^{n+1} = & u^n + \Delta t \partial_t u^n + \Delta t^2 \partial_{tt} + o(\Delta t^3) \\
			= & u^n+  \Delta t ( \partial_t u^n + \Delta t \partial_{tt} + o(\Delta t^2)) \\
			= & u^n + \Delta t \partial_t (u^n + \Delta t \partial_t u^n + o(\Delta t^2)) \\
			= & u^n + \Delta t \partial_t  (u^n + 2\frac{\Delta t}{2}\partial_t u^n + o(\Delta t^2)) \\
			= & u^n + \Delta t \partial_t (u^{n+\frac{1}{2}} + \frac{\Delta t}{2} \partial_t u^n + o(\Delta t^2)) \\
			= & u^n + \Delta t \partial_t u^{n+\frac{1}{2}} + \frac{\Delta t^2}{2}\partial_{tt} u^n + o(\Delta t^3) \\
			 = & u^n + \Delta t \partial_t u^{n+\frac{1}{2}} + o(\Delta t^2) \quad \quad \forall x_i \in \{1, ...,Nx\}
		\end{align*}
		
		which proove that RK2 is a 2nd order scheme in time.
		
	\subsection{Numerical implementation}
		
		By using Runge Kutta 2 method, we discretize our equation (\ref{advdiff}) in time, and we use a finite difference method to discretize $\partial_x u(t, x)$ and  $\partial_{xx} u(t, x)$:
		
		We define $0 = t_0 \leq t_1 \leq t_2 \leq ... \leq t_k = T$ the time discretization and $-1 = x_0 \leq x_1 \leq ... \leq x_m = 1$ the space discretization. With these notation, we can rewrite our equation:
		
		
		\begin{align*}
			& \partial_t u + a \partial_x u - \nu^2 \partial_{xx}u = 0\\
			\Longleftrightarrow & \partial_t u(t, x_i) + a \frac{u(t, x_{i+1}) - u(t, x_{i-1}}{2 \Delta x} - \nu^2 \frac{ u(t, x_{i+1}) - 2u(t, x_i) + u(t, x_{i-1}}{\Delta x^2} =0 \\
			\Longleftrightarrow &  \partial_t u(t, x_i) = - a \frac{u(t, x_{i+1}) - u(t, x_{i-1}}{2 \Delta x} + \nu^2 \frac{ u(t, x_{i+1}) - 2u(t, x_i) + u(t, x_{i-1})}{\Delta x^2}			
		\end{align*}		
		
		
		We consider $f(t^n, x_i)= - a \frac{u(t, x_{i+1}) - u(t, x_{i-1})}{2 \Delta x} + \nu^2 \frac{ u(t, x_{i+1}) - 2u(t, x_i) + u(t, x_{i-1}}{\Delta x^2}$ so $\partial_t u(t^n, x_i) = f(t^n, x_i)$ and, with the RK2 method, we finally have:
		
		\begin{align*}
			u_i^{n+1} & = u_i^n + \Delta x \partial_t u_i^{n+\frac{1}{2}} \\
					& = u_i^n + \Delta x ( u_i^n + \frac{\Delta x}{2} (- a \frac{u_{i+1}^n - u_{i-1}^n}{2 \Delta x} + \nu^2 \frac{ u_{i+1}^n) - 2u_i^n + u_{i-1}^n}{\Delta x^2}) )
		\end{align*}
		
		\medskip
		
		We get a new explicit system, we will implement 
		
		\begin{equation}
			\label{discretadvdiff}
			\left\{
			\begin{array}{llll}
				u_i^{n+1} =  u_i^n + \Delta x ( u_i^n + \frac{\Delta x}{2} (- a \frac{u_{i+1}^n - u_{i-1}^n}{2 \Delta x} + \nu^2 \frac{ u_{i+1}^n) - 2u_i^n + u_{i-1}^n}{\Delta x^2}) )\\
				\Omega = ] -3;3 [ \\
				a = 1 \\
				\nu = 0.01 \\
				u_0(x) = u(x, 0) = t^2
			\end{array}
		\right.		
		\end{equation}
		
	
	%-----------------------------------------------------------------------
	\subsection{Error scheme}
	
		We have seen that RK2 method is a 2nd order in time. Besides, we have a 2nd order in space too, with the finite difference method
		
		\begin{proof}
			\smallskip
			We should start by reminding us the Taylor expansion of $u_{i+1}^n$ and $u_{i-1}^n$:
			
			\begin{align*}
				u_{i+1}^n = u(t^n, x_{i+1}) = u_i^n + \Delta x \cdot \partial_x u_i^n + \frac{\Delta x^2}{2} \partial_{xx} u_i^n + \frac{\Delta x^3}{6} \partial_{xxx} u_i^n + \mathcal{O} (\Delta x^4)\\
				u_{i-1}^n = u(t^n, x_{i-1}) = u_i^n - \Delta x \cdot \partial_x u_i^n + \frac{\Delta x^2}{2} \partial_{xx} u_i^n - \frac{\Delta x^3}{6} \partial_{xxx} u_i^n + \mathcal{O} (\Delta x^4)
			\end{align*}
			\medskip
			So we can deduce that: 
			
			\begin{align*}
				u_{i+1}^n - u_{i-1}^n &  = 2 \Delta x \partial_x u^n_i + 2 \frac{\Delta x^3}{6} \partial_{xxx} u^n_i + \mathcal{O} (\Delta x^4) \\ 
				\frac{u_{i+1}^n - u_{i-1}^n}{2 \Delta x} & = \partial_x u_i^n + \frac{\Delta x^2}{6} + \mathcal{O}(\Delta x^3) \\
				\partial_x u_i^n = \frac{u_{i+1}^n - u_{i-1}^n}{2 \Delta x} + \mathcal{O}(\Delta x^2)
			\end{align*}
			
			\medskip
			In the same way, we have:
			
			\begin{align*}
				u_{i+1}^n - 2u_i^n + u_{i-1}^n &= 2u_i^n + \Delta x^2 \partial_{xx} u_i^n + \mathcal{O} (\Delta x^4) - 2u_i^n \\
				\frac{u_{i+1}^n - 2u_i^n + u_{i-1}^n}{\Delta x^2} &= \partial_{xx}u_i^n + \mathcal{\Delta x^2} \\
				\partial_{xx}u_i^n &= \frac{u_{i+1}^n - 2u_i^n + u_{i-1}^n}{\Delta x^2}+ \mathcal{O} (\Delta x^2)
 			\end{align*}
			
		\end{proof}
		
		\bigskip
		
		
		
		%-----------------------------
		As we find our scheme is in $\mathcal{O}(\Delta t^2)+ \mathcal{O}(\Delta x^2)$, we can imagine the error between the approximate and the analytical solution will in second order. 
		
		We would like to see the evolution of the error according to the number of discretization, that's why we will calculate the error for several number of space discretization. 
		
		\smallskip
		
		We consider $N_x$ the first number of space discretization on [a, b], so $\Delta x = \frac{b-a}{N_x}$. We choose to define $\Delta t = \frac{\Delta x}{2}$, so :
		
		\[
			2 N_x \Longrightarrow \Delta x = \frac{b-a}{2 N_x} \Longrightarrow \Delta t = \frac{\Delta x}{4}
		\]
		
		So if we multiply by 2 $N_x$, we can see that our error will be in $\mathcal{O}((\frac{\Delta x}{2})^2) + \mathcal{0}((\frac{Delta t}{2})^2) = \frac{1}{4}(\mathcal{O}(\Delta t^2)+ \mathcal{O}(\Delta x^2))$
		
				
		\smallskip
		\begin{figure}[!h]
			\centering
			\includegraphics[scale=0.6]{img/errorSchemeCurve.png}
			\caption{Representation of this error of our approximation solution}
		\end{figure}
	
	On this figure, we can see that the order of our scheme is parallel to the line of order 1. However, we expected to get a line parallel to the slope of order 2. So we can suppose we did some mistakes in our program.
	
	
	%------------------------------------PARTIE LDC -----------------------------------%
	\newpage	
	\section{LDC scheme}
	
	In this part, we want to study the next equation:
	
	\begin{equation}
		\label{systeme}
		\left\{
			\begin{array}{ll}
				u" = f \quad \Omega = ]0;1[ \\
				u(0) = u_0, \; u(1) = u_1 
			\end{array}		
		\right.
	\end{equation}
	
	with f, $u_0$ et $u_1$ define by the analytical solution :
	
	\begin{equation}
		\label{solAnalytique}
		u(x) = \frac{1}{2} (\tanh(50(x-\frac{1}{8})) + 1)
	\end{equation}
	
	As we have seen a large variation between analytical and approximal solution in  $x = \frac{1}{8}$, we will be particulary interested by the neighbour of this point by using a fine mesh around  $x = \frac{1}{8}$.
	
	
	
	% -------------------------------------------------------------------
	\subsection{Exercise 1: finite difference method order 2}
	
	
		\subsubsection{Discretization of u}
	
			We use the fourth order Taylor expansion: 
	
			\begin{align*}
				u(x+ h) & = u(x) + dx u'(x) + \frac{dt^2}{2}u''(x) + \frac{dt^3}{6}u^{(3)}(x) +\mathcal{O} (dt^4) \\
				u(x - h) &= u(x) - dx u'(x) + \frac{dt^2}{2}u''(x) - \frac{dt^3}{6}u^{(3)}(x) + \mathcal{O} (dt^4)
			\end{align*}
		
		
			And if we add both we get:
			
			\begin{equation*}
				u(x+h) + u(x- h) = 2u(x) + dt^2 u''(x) + \mathcal{O}(dt^4)
			\end{equation*}
			
			Finally
	
			 \begin{equation}
			 	u''(x) = \frac{u(x+h) - 2u(x) + u(x-h)}{dx^2} + \mathcal{O}(dx^2)
			 \end{equation}


		\subsubsection{Numerical resolution}
	
			We already know boundaries conditions u(0) and u(1), which are done by the analytical solution and we will deduce from (\ref{solAnalytique}) the second member of the equation f:
		
			\begin{equation}
				f(x) = u''(x) = - 25 \times 50 \times 2 \times (1 - \tanh^2(50x - \frac{50}{8})) \times \tanh(50x - \frac{50}{8}) 
			\end{equation}
		
		
			\bigskip
			By using $H = \frac{1}{20}$ as space step, we get this numerical solution:
		
		
			%\begin{figure}[!ht]
				%\centering
				\begin{minipage}[t]{0.40\linewidth }
					\centering
					\includegraphics[scale=0.4]{exo1/figures/ComparaisonExacteApprochee1.jpg}
					%\caption{Numerical solution of (\ref{systeme}) with $H=\frac{1}{20}$ }
				\end{minipage}
				\hfill
				\begin{minipage}[t]{0.40\linewidth}
					\includegraphics[scale=0.4]{exo1/figures/ComparaisonExacteApprochee2.jpg}
					%\caption{Numerical Solution of (\ref{systeme}) with $h=\frac{1}{80}$ }
				\end{minipage}
			%\end{figure}
			
			\smallskip
			In the same way, on find the numerical solution with $h = \frac{H}{4} = \frac{1}{80} $as new space step 
		
		
			\bigskip
			
			We can see on the first figure there is a significant variation between the analytical solution and the approximate solution in the neighborhood of $x=\frac{1}{8}$. But this variation is less significant 	on the second figure where our space step h is smaller.
			
	
	
	%----------------------------------------------------------------------------------------------
	\subsection{Exercise 2: LDC scheme}
	
		In this part we would like to combine the coarse mesh and the fine mesh we used before to have a good approximation of the solution and a better optimization of our program: it is the LDC method.
		
		At the end of our program we have this graph:
		
		\begin{figure}[!h]
			\centering
			\includegraphics[scale=0.5]{img/LDC_scheme.png}
			\caption{Representation of LDC scheme}
		\end{figure}
	
	On this figure, we can see that our final approach solution don't look as the analytical soution, as it sould be. I can suppose the problem come from the Dirichlet boundary condition in the fine mesh because our value of $u^H_l (\gamma) $ don't change over time.
	
	
\end{document}
