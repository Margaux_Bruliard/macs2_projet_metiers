%% LDC METHOD
% Aeronautic Project


%% ---------------- DATA ----------------- %%
u_analytic = @(x) (tanh(50*(x-1/8)+1)/2);

f = @(x) (1250*2*((u_analytic(x)*2).^3-(u_analytic(x)*2)));

a = 0;
b = 1. ;
gamma = 0.3 ;

rapport = 4;
H = 1/20 ;
h = H/rapport ;

u0 = u_analytic(a);
u1 = u_analytic(b) ;

maxIter = 10 ;
tol = 1e-10 ;

coarseMesh = a:H:b ;
fineMesh = a:h:gamma ;

F_initial = f(coarseMesh') ;
ind_gamma_coarse = 1 + int8(gamma/H);

%% ---------------- STEP A : COARSE MESH ------------ %%
F = f(coarseMesh') ;
u_coarse = poissonSolver1D ( coarseMesh, H,  F, u0, u1) ;


%% ---------------- STEP B:  FINE MESH ---------------- %%
F_fineMesh = f(fineMesh');
u_fine = poissonSolver1D (fineMesh, h, F_fineMesh, u0, u_coarse(ind_gamma_coarse)) ;


 % -- Compute the first error dx
d = zeros(size(F_initial)) ;

for i = 2:ind_gamma_coarse-1
    d(i) = (u_fine(1 + i*rapport) - 2*u_fine(1+(i-1)*rapport) + u_fine(1 + (i-2)*rapport))/ (H^2) - F_initial(i) ;
end

%% ---------------- STEP C : NEW COARSE MESH --------------------- %%
F
F = F + d;
F
new_u_coarse = poissonSolver1D (coarseMesh, H, F, u0, u1);


% hold on
% plot(a:H:b, u_coarse, 'b');
% plot(fineMesh, u_fine, 'r');
% plot(coarseMesh, u_analytic(coarseMesh), 'g') ;
% plot(coarseMesh, u_coarse, 'p');
% legend({'coarse Mesh', 'Fine Mesh', 'analytic', 'new u coarse'}) ;
% hold off

% ---
it = 1 ;
err = max(abs(u_coarse - u_analytic(coarseMesh')))

while (it < maxIter) && (err > tol)
    u_coarse = new_u_coarse ;
    
    disp(fprintf('it= %d \n val_sur gamma: %g', it, u_coarse(ind_gamma_coarse)));
    u_fine = poissonSolver1D (fineMesh, h, F_fineMesh, u0, u_coarse(ind_gamma_coarse)) ;
    
     % -- Compute the first error dx
    d = zeros(size(F)) ;

    for i = 2:ind_gamma_coarse-1
        d(i) = (u_fine(1 + i*rapport) - 2*u_fine(1+(i-1)*rapport) + u_fine(i + (i-2)*rapport) ) / (H^2) - F_initial(i) ;
    end
    d
    F = F_initial + d;
    new_u_coarse = poissonSolver1D (coarseMesh, H, F, u0, u1);
    
    
    err = max(abs(new_u_coarse - u_coarse))
    it= it+1;
    
end


hold on
plot(a:H:b, u_coarse, 'b');
plot(fineMesh, u_fine, 'r');
plot(coarseMesh, u_analytic(coarseMesh), 'g') ;
plot(coarseMesh, new_u_coarse, 'p');
legend({'coarse Mesh', 'Fine Mesh', 'analytic', 'new u coarse'}) ;
hold off















