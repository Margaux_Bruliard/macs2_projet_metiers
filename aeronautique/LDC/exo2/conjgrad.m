function [x] = conjgrad(A,b,x0,epsi)
r=b - A*x0;
w=-r;
z=A*w;
a=(r'*w)/(w'*z);
x=x0 + a*w;
% B=0;

n=numel(A(1,:));
for i = 1:n
    r=r-a*z;
    if r<epsi
        break;
    end
    B=(r'*z)/(w'*z);
    w=-r+B*w;
    z=A*w;
    a=(r'*w)/(w'*z);
    x=x + a*w;
end
end