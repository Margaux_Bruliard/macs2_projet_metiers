function [ u ] = poissonSolver1D( interval, gap, F, dirichlet_gauche, dirichlet_droite )
%--- Solve a poisson equation 1D : $ \Delta u = f $
%   Input:
%       @interval is a [a, b] intervalle of the domain
%       @gap is the gap between two elements of interval
%       @F is the second member f discretized
%       @dirichlet_gauche, @dirichet_droite are boundary conditions
%   Output:
%       @u vector solution of poisson equation

    dx2 = gap^2 ;
    n = length(interval) ;
    
    A = sparse(n, n);
    A(2:n+1:(n-1)^2)=1/dx2;
    A(n+2:n+1:n^2-1)=-2/dx2;
    A(2*n+2:n+1:n^2)=1/dx2;
    A(1)=1;
    A(end)=1;

    
    F(1) = dirichlet_gauche; 
    F(n) = dirichlet_droite;
    
    u = A\F ;

end

