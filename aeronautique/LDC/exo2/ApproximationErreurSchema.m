function [ d ] = ApproximationErreurSchema( u , pas_schema, f)
%Permet de calculer l'erreur du schéma grossier
%   Données:
%       @u est le vecteur solution sur le schéma fin
%       @pas est le pas du schéma grossier
%       @f est le second membre discrétisé
%   Résultat:
%       @d le vecteur erreur 

d = zeros(length(u), 1);


for i =2:length(u)-1
    d(i) = ( u(i+1) - 2*u(i) + u(i-1))/(pas_schema^2) - f(i);  
end
% 
% disp('d')
% disp(d)
% pause(1)
end

