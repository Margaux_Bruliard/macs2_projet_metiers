function [ u ] = SolveLaplacienDirichlet( x_min, x_max, pas_temps, F, condDirichletGauche, condDirichletDroite )

%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

    pt2 = pas_temps^2 ; 
    x = (x_min:pas_temps:x_max)';
    n = length(x) ; % le nombre de discrétisations pour le schéma grossier
    
    A = sparse(n, n);
    A(2:n+1:(n-1)^2)=1/pt2;
    A(n+2:n+1:n^2-1)=-2/pt2;
    A(2*n+2:n+1:n^2)=1/pt2;
    A(1)=1;
    A(end)=1;

    % on ajoute ensuite les conditions aux bords

    F(1) = condDirichletGauche; %conditions de Dirichlet
    F(n) = condDirichletDroite;
    
    
   

    % Résolution
    u = A\F;

end

