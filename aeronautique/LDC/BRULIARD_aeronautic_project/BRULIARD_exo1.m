%% TP2 : AERONAUTIQUE - LDC
% Exercice 1 Résolution du problème avec un maillage régulier sur un schéma
% différences finies centré d'ordre 2


a = 0; %domaine Omega
b = 1;



u_analytique = @(x) (0.5* (tanh(50*(x-1/8)) + 1)); %solution analytique


f = @(x) ( -25 * 50 * 2 * (1 - (tanh(50*(x - 1/8))).^2).*tanh(50*(x - 1/8))); % derivée seconde de la solution analytique

u0 = u_analytique(0);
u1 = u_analytique(1);


%% 

h = 1/80; % choix du pas d'espace
X = (a:h:b)';
n = length(X) ; % le nombre de discrétisations


% on construit le vecteur du second membre
F = f(X);


% on construit la matrice de rigidité telle que :
% AX = F

A = sparse(n, n);
A(1:n+1:n^2) = -2/(h^2);
A(2:n+1:n^2 - n) = 1/(h^2);
A(n+1:n+1:n^2- 1) = 1/(h^2);


% on ajoute ensuite les conditions aux bords
F(1) = u0;
F(n) = u1;
A(1) = 1; 
A(n+1:n:n^2) = 0;
A(n^2) = 1;
A(n:n:n^2 - n) = 0;

%disp(A);

U = A\F;


plot (X, U, 'b', X, u_analytique(X), 'r')
legend({'solution approchée', 'solution exacte'});
title (sprintf('Schéma différences finies ordre 2 pour h=%d', h));






