%% TP 2 Aeronautique :  LDC


H = 1/20;
h = 1/80;

rapportHh = int8(H/h);

a = 0;
b = 1;

gamma = 3/10;

x_grossier = a:H:b ;
Nx_grossier = length(x_grossier);
ind_gamma = int8(1 + gamma/H) ;


x_fin = a:h:gamma ; 
Nx_fin = length(x_fin);

%u_analytique = @(x) (0.5* (tanh(50*(x-1/8)) + 1)); %solution analytique
%f = @(x) ( -25 * 50 * 2 * (1 - (tanh(50*(x - 1/8))).^2).*tanh(50*(x - 1/8)));

u0 = u_analytique(0);
u1 = u_analytique(1);

u_check = u_analytique(3/10)

epsilon_value = 1e-15;
Nit = 1000;
erreur = 1;

F_initial = terme_source(x_grossier');
F_schemaFin = terme_source(x_fin');

%% Etape A: Résolution du schéma grossier
hold on
u_schemaGrossier = SolveLaplacienDirichlet ( a, b, H, F_initial, u0, u1);
plot(x_grossier, u_schemaGrossier, 'g');

F = F_initial ;
it = 1;
while (erreur > epsilon_value) & (it < Nit)
     
    %% Etape b: Calcul de l'erreur pour l'itération it sur le shéma fin

    % --- on construit un maillage fin sur [0, gamma]
    condDirichlet_a_droite_u_fin = u_schemaGrossier(ind_gamma);
    %condDirichlet_a_droite_u_fin = u_analytique(gamma);
    condDirichlet_a_gauche_u_fin = u0; %u_schemaGrossier(1);
    u_schemaFin = SolveLaplacienDirichlet(a, gamma, h, F_schemaFin, condDirichlet_a_gauche_u_fin, condDirichlet_a_droite_u_fin);
    
    
    % --- calcul de l'erreur d
    u_error = u_schemaGrossier;
   
    % --------- on place dans u_error les nouvelles valeurs issus du
    % maillage fin
    for i=1:ind_gamma
        u_error(i) = u_schemaFin(1 + rapportHh*(i-1));
    end

    d= ApproximationErreurSchema(u_error, H, F);

    %% Etape 3: Résolution du système adapté
    
    F = F+d;
    u_schemaGrossier_suiv = SolveLaplacienDirichlet (a, b, H, F, u0, u1);
    
    %erreur = norm(u_schemaGrossier_suiv - u_schemaGrossier);
    erreur = max(abs(u_schemaGrossier_suiv - u_schemaGrossier));
     
    
    
    u_schemaGrossier = u_schemaGrossier_suiv;
    it = it + 1;
end

disp('Nombre d iterations:')
disp(it);

disp('Erreur finale:');
disp(erreur);

plot (a:H:b, u_schemaGrossier, 'b', a:H:b, u_analytique(a:H:b), 'r')
legend({'initial approach solution', 'final approach solution', 'analytical solution'});
title (sprintf('LDC scheme in order 2 with h=%d', H));

hold off
