function [ t , x , u ] = EulerExplicite( EDP , Nt , Nx )
% EulerExplicite --- euler solver for the 1D convection equation
%   [ t , x , u ] = EulerExplicite( EDP , Nt , Nx )

% Time and space discretisation
dt = EDP.T/(Nt-1);
dx = (EDP.b-EDP.a)/Nx;
t  = 0:dt:EDP.T;
x  = EDP.a:dx:EDP.b;

% Setting initial solution
u = EDP.u0(x);
min_val = min(u);
max_val = max(u);

u_tmp = zeros(1,Nx+1);


for n = 1:Nt
    u_old = u;

    % Boundary conditions
    u(1) = EDP.ua(t(n));
    u(Nx+1) = EDP.ub(t(n));

    for i = 2:Nx       
        u_advection = - 0.5*EDP.advection_speed * (u_old(i+1)-u_old(i-1)) / dx;
        u_diffusion = (u_old(i+1)-2*u_old(i)+u_old(i-1)) / dx^2;     
        u_tmp(i) = u_old(i) + dt * (u_advection + u_diffusion) / 2;
    end

    u_tmp(1) = EDP.ua(t(n));
    u_tmp(Nx+1) = EDP.ub(t(n));
    
    for i = 2:Nx
        u_advection = - 0.5 * (u_tmp(i+1)-u_tmp(i-1)) / dx;
        u_diffusion = (u_tmp(i+1)-2*u_tmp(i)+u_tmp(i-1)) / dx^2;
        u(i) = u_old(i) + dt * (u_advection + u_diffusion);
    end

    plot(x, u)
    axis([x(1) x(Nx+1) min_val max_val])
    title(sprintf('Time = %f', t(n)))
    pause(0.00001)
end

end
