clear all
close all

PLOT = 1;
FREQ = 10;

% 1. Initialisation de la structure EDP
EDP.a               = -pi;
EDP.b               = pi;
EDP.t0              = 0;
EDP.T               = 5.0;
EDP.nu              = 0.01;
EDP.advection_speed = 1.0;

R = 5.0;

EDP.u0 =@(x) exp(-R*x.^2);
EDP.ua =@(t) 0.05*(cos(2*t)+1);
EDP.ub =@(t) 0.05*(cos(2*t)+1);
EDP.f  =@(t,x) 0;

% 2. Parametres de discretisation
Nx = 200;
Nt = 50000;

% % 3. Verification de la condition de C.F.L.
% ht  = EDP.T/Nt; hx=(EDP.b-EDP.a)/Nx;
% CFL = EDP.nu*ht/(hx^2);
% s   = sprintf('CFL : si %f < 0.5 => convergence',CFL);
% disp(s);

% 4. Resolution par le schema d'EULER explicite
disp('Calcul en cours...')
[t,x,u] = EulerExplicite(EDP,Nt,Nx);
disp('Fin du calcul.')

% % 5. Representations graphiques
% MIN = min(min(u));
% MAX = max(max(u));
% if PLOT
%     figure(1)
%     PlotSol2(t,x,u,'freq',FREQ,'title','Sol. Appr.','axis',[x(1) x(end) MIN MAX],'pause',0.1)
%     % PlotSol(t,x,u,FREQ,'Sol. Appr.',[x(1) x(end) MIN MAX],0.1) % Old version
% end

