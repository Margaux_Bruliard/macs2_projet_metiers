function [ v ] = diffRK2( u, coeff , dx)
%Calcul au temps n de la discrétisation d'ordre 2 d'une équation de
%diffusion
%   On se place ici dans le cas de conditions aux bords périodiques
%   Données:
%       @dx est le pas en espace
%       @u  est le vecteur solution au temps n
%       @coeff  est le coefficient de de diffusion
%   Résultat:
%       @v est le vecteur discrétisation 
%           $$ (v)_i = - coeff^2 \cdot \frac{u_{i+1} - 2u_i + u_{i-1}}{dx^2} $$

n= length(u);
dx2 = dx^2;


v = zeros(n, 1);


v(1) = coeff^2 * (u(2) - 2*u(1) + u(n))/dx2;
v(n) = coeff^2 * (u(1) - 2*u(n) + u(n-1))/dx2;
%v(n) = v(1);

for i = 2:n-1
    v(i) = coeff^2 * (u(i+1) - 2*u(i) + u(i-1))/dx2;
    
end







end

