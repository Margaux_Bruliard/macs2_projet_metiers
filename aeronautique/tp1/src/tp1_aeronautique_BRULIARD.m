% TP 1 : AERONAUTIQUE
% We study the advection-diffusion equation with periodic boundary
% conditions
% 
% 
% $$ \partial_t u + a \partial_x u - \nu^2 \partial_{xx} u = f$$
% 
% To verify our program, we choose the exact solution : 
% $ u(t, x) = x^2 + t^2 $
% So we know that $ f(x) =  2t + 2ax - 2\nu^2 $ où  a est le coefficient
% d'advection et $\nu$ est le coefficient de diffusion


clear all
close all

%% DONNEES DU PROBLEME
a = -3;
b = 3;
adv_speed = 1.0;
coef_diff = 0.1; %nu
R = 5;

T = 1;
Nx = [50, 100, 200, 400, 600];


uex = @(t, x)  (t^2 + x.^2);

partial_t_uex = @(t, x) (2*t);
partial_x_uex = @(t, x) (2*x);
partial_xx_uex= @(t, x) (2);


f = @(t, x) ( partial_t_uex(t, x) + adv_speed*partial_x_uex(t, x) - coef_diff^2 *partial_xx_uex(t, x)); 


%%  
for j = 1:length(Nx)
    
    nx = Nx(j);
    h = (b-a)/nx;
    dt = 0.5*h;
    Nt = T/dt;
    t = 0:dt:T;
    x = (a:h:b-h)';

    err = zeros(1, length(t));
    err(1) = 0;
    k = 2;

    
    u = uex(0, x);
    temps = 0;
    
    while temps < T
   
        v1 = advection(u, adv_speed, h);
        v2 = diffRK2 ( u,coef_diff, h);
        F = f(temps, x); 

        u_undemi = u + 0.5*dt*(F + v1 + v2); %y_n+1/2 = yn + h/2*f(tn, y_n)

        % ------ yprime_n+1/2 = f(y_n+1/2)
        v1 = advection(u_undemi, adv_speed, h);
        v2 = diffRK2(u_undemi, coef_diff, h );
        F = f(temps + dt/2, x);

        u = u + dt*(F + v1 + v2); %y_n+1 = y_n + h*yprime_n+1/2
        u(1) = uex(temps+dt, a);
        u(length(u)) = uex (temps+dt, b);
        
%         plot(x, u, 'b', x, uex(temps + dt, x), 'r') ;
%         axis([a, b, 0, 9]);
%         legend({'solution approchée', 'solution exacte'}, 'Location', 'southwest');
%         title(sprintf('advection-diffusion solution\nNx=%d \ndt=%d, dx=%d, t=%d', nx, dt, h, temps+dt))
%         pause(0.01)

                
        temps = temps + dt;
    end
    
    pause(0.1);
    %Err(j) = sum(err)/length(err);
    Err(j) =max(abs(uex(T, x) - u));
end


loglog (Nx, Err, 'b', Nx, (1./(Nx)), 'c', Nx, (1./Nx.^2), 'g' );
legend({'order of scheme error', 'order 1', 'order 2'}, 'Location',  'southwest');
xlabel('Number of space discretization Nx');
ylabel('error');
title('Error of the scheme in logarithmic scale')








