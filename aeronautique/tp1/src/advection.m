function [ v1 ] = advection( u, a , dx)
%Calcul au temps n de la discrétisation d'ordre 2 d'une équation d'advection
%   On se place ici dans le cas de conditions aux bords périodiques
%   Données:
%       @dx est le pas en espace
%       @u  est le vecteur solution au temps n
%       @a  est le coefficient de transport de l'advection
%   Résultat:
%       @v1 est le vecteur discrétisation 
%           $$ (v1)_i = - \frac{a}{2} \cdot \frac{u_{i+1} - u_{i-1}}{dx} $$


n = length(u);
v1 = zeros(n, 1);

v1(1) = - 0.5*a*(u(2) - u(n))/dx;
v1(n) =- 0.5*a*(u(1) - u(n-1))/dx;
%v1(n) = v1(1);

for i = 2:n-1
    v1(i) = - 0.5*a*(u(i+1) - u(i-1))/dx;
end

end

