\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage{color}
\usepackage{amsfonts}
\usepackage{dsfont}
\usepackage{amssymb}
\usepackage{calc}


% -------------------------- INFOS ------------------------- %
\date{\today}
\author{Margaux BRULIARD \\ MACS 2 - Sup'Galilée}
\title{Simulation des écoulements diphasiques incompressibles en milieux poreux}


\newcommand{\divergence}{\overrightarrow{div}}
\newcommand{\gradient}{\overrightarrow{\nabla}}
\newcommand{\vitesse}{\overrightarrow{U}}



% ------------------------ DOCUMENT START ------------------ %
\begin{document}

	\maketitle
	\hspace*{3cm} \hrulefill \hspace*{3cm}
	\vspace*{2cm}
	
	On s'intéresse au modèle mathématique suivant:
	
	\begin{equation}
		\label{modelBase}		
		\left \{
			\begin{array}{lll}
				\phi \partial_t S_w + \divergence(- \frac{k r_w(S_w)}{\mu_w} \overline{K} \gradient P ) = 0 \quad \Omega \times [0, T] \\
				 \phi \partial_t S_o + \divergence(- \frac{k r_o(S_o)}{\mu_o} \overline{K} \gradient P ) = 0 \quad \Omega \times [0, T] \\
				 S_w + S_o = 1		
			\end{array}
		\right.
	\end{equation}


	
	
	% --------------------- REECRITURE DU SYSTEME ----------------- %
	\section{Réécriture du système [\ref{modelBase}] en un système à deux équations:}
	
	
	Nous allons utiliser la formule $ S_w + S_o = 1 $  de [\ref{modelBase}] et l'injecter dans la seconde formule du système. On obtient alors:
	\begin{equation*}	
		\left \{
			\begin{array}{lll}
				\phi \partial_t S_w + \divergence(- \frac{k r_w(S_w)}{\mu_w} \overline{K} \gradient P ) = 0 \quad \Omega \times [0, T] \\
				 \phi \partial_t S_o + \divergence(- \frac{k r_o(S_o)}{\mu_o} \overline{K} \gradient P ) = 0 \quad \Omega \times [0, T] \\
				 S_w + S_o = 1		
			\end{array}
		\right.
	\end{equation*}
	
	\smallskip 
	
	\begin{equation*}	
		\left \{
			\begin{array}{lll}
				\phi \partial_t S_w + \divergence(- \overline{K} \frac{k r_w(S_w)}{\mu_w} \gradient P ) = 0 \quad \Omega \times [0, T] \\
				 \phi \partial_t S_o + \divergence(- \overline{K} \frac{k r_o(S_o)}{\mu_o} \gradient P ) = 0 \quad \Omega \times [0, T] \\
				 S_w + S_o = 1		
			\end{array}
		\right.
	\end{equation*}
	
	
	
	Nous allons utiliser la notation de la la vitesse donnée par la loi de Darcy: 
	\begin{equation}
		\label{defVitesse}
		\left\{
			\begin{array}{lll}
				\vitesse_w = - \overline{K} \frac{k r_w(S_w)}{\mu_w} (\gradient P) \\
				\vitesse_o = - \overline{K} \frac{k r_o(S_o)}{\mu_o} (\gradient P) \\
				\vitesse_t = \vitesse_o + \vitesse_w = - M_t(S)\overline{K}\gradient P
			\end{array}
		\right.
	\end{equation}
	
	
	d'où on obtient la transformation de (\ref{modelBase}) avec la loi de Darcy
	\begin{equation*}	
		\left \{
			\begin{array}{lll}
				\phi \partial_t (1- S_o) + \divergence(\vitesse_w) = 0 \quad \Omega \times [0, T] \\
				 \phi \partial_t S_o + \divergence(\vitesse_o ) = 0 \quad \Omega \times [0, T] \\
				 S_o = 1 - S_w		
			\end{array}
		\right.
	\end{equation*}
	
	\begin{equation*}	
		\left \{
			\begin{array}{lll}
				- \phi \partial_t S_o + \divergence(\vitesse_w) = 0 \quad \Omega \times [0, T] \\
				 \phi \partial_t S_o + \divergence(\vitesse_o ) = 0 \quad \Omega \times [0, T] \\
				 S_o = 1 - S_w		
			\end{array}
		\right.
	\end{equation*}
	
	et en sommant les deux équations ci-dessus, on obtient:
	\begin{equation*}	
		\left \{
			\begin{array}{lll}
				\divergence(\vitesse_o + \vitesse_w) = \divergence (\vitesse_t) = 0 \quad \Omega \times [0, T] \\
				 \phi \partial_t S_o + \divergence(\vitesse_o ) = 0 \quad \Omega \times [0, T]
			\end{array}
		\right.
	\end{equation*}
	
	\smallskip
	On a vu dans (\ref{defVitesse}) que : 
	
		\[
			\vitesse_o = - \overline{K} \underbrace{\frac{k r_o(S_o)}{\mu_o}}_{=M_o(S_o)} (\gradient P)
		\]
		
		où $M_o$ est la mobilité de la phase huile. De la même façon nous pouvons définir la mobilité de la phase eau $M_w$ : $M_w (S_w) = \frac{kr_w(S_w)}{\mu_w}$, donc on définit la mobilité totale comme:
		
		\begin{align*}
			M_t (S_o) &= M_w (S_w) + M_o(S_o) \\
				&= M_w(1-S_o) + M_o(S_o)  
		\end{align*}
	
	
		et comme on sait que 
		
		\begin{align*}
			\vitesse_t &= \vitesse_o + \vitesse_w \\
				&= - (M_o(S_o) + M_w(S_w)) \overline{K} \gradient P \\
				&= - M_t(S_o) \overline{K} \gradient P
		\end{align*}
		
		\smallskip
		Nous pouvons alors réécrire $\vitesse_o$:
		
		\begin{align*}
			\vitesse_o &= -M_o(S_o) \overline{K} \gradient P \\
				&= - M_o(S_o) \frac{M_t(S_o)}{M_t(S_o)} \overline{K}\gradient P \\
				&= \frac{M_o(S_o)}{M_t(S_o)} \vitesse_t \\
				&= \frac{M_o(S)}{M_w(1-S_o) + M_o(S_o)}\vitesse_t \\
		\end{align*} 
		
	\bigskip
	Finalement, on a bien un système de deux équations à deux inconnues qui sont la pression P et la saturation d'huile $S_o$
	
	\begin{equation}
		\label{sytemModifie}	
		\left \{
			\begin{array}{ll}
				\divergence (\vitesse_t) = 0 \quad \Omega \times [0, T] \\
				\phi \partial_t S_o + \divergence(\frac{M_o(S)}{M_w(1-S_o) + M_o(S_o)}\vitesse_t ) = 0 \quad \Omega \times [0, T]
			\end{array}
		\right.
	\end{equation}




	% ------------------------ ECRITURE SCHEMA VOLUMES FINIS -------------- % 
	\section{Ecriture du schéma volume finis pour le système (\ref{sytemModifie})}
	
	Nous allons discrétiser en temps la seconde équation du système à l'aide des développements de Taylor à l'ordre 1. Ainsi
	
	\[
		\partial_t S_o \approx \frac{S_o^{n+1} - S_o^n}{t^{n+1} - t^n}
	\]
	
	où $S_o^n$ représente la saturation en huile au temps $t^n$. De plus on notera par la suite $\Delta t = t^{n+1} - t^n $ et comme nous étudions uniquement la saturation en huile, on considerera $S = S_o$.
	
	Ainsi notre système (\ref{sytemModifie}) peut être discrétisé comme suit:
	\begin{equation*}
		\left\{
			\begin{array}{ll}
				\divergence (\vitesse_t) = 0 \\
				\phi \frac{s^{n+1} - S^n}{t^{n+1} -t^n} + \divergence(\frac{M_o(S)}{M_w(1-S) + M_o(S)}\vitesse_t ) = 0 
			\end{array}
		\right.
	\end{equation*}
	
	\bigskip
	Or nous avons $\vitesse_t$ qui dépend de la pression et de la saturation en huile, aussi pour découpler le calcul de ces deux paramètres nous allons mettre en place un \textbf{schéma IMPES} \footnote{IMplicite sur le premier paramètre et Explicite sur le second}.
	
	\begin{equation*}
		\left\{
			\begin{array}{ll}
				\divergence (\vitesse_t(P^{n+1}, S^n)) = 0 \\
				\phi \frac{s^{n+1} - S^n}{t^{n+1} -t^n} + \divergence(\frac{M_o(S^n)}{M_w(1-S^n) + M_o(S^n)}\vitesse_t(P^{n+1}, S^n) ) = 0 
			\end{array}
		\right.
	\end{equation*}
	
	Ce schéma étant conditionnellement stable, nous veillerons à déterminer sa \textbf{condition CFL} au cours de ce projet.
	
	\bigskip
	Commençons par discrétiser la première équation afin de déterminer \textbf{la pression}. En intégrant chaque cellule K du maillage $\mathcal{M_h}$,
	
	\[
		\int_K \divergence(\vitesse_t (P^n, S^{n-1}))dx
	\]
	
	et en utilisant la formule de Green sur la divergence alors
	
	\begin{align*}
		\int_K \divergence(\vitesse_t (P^n, S^{n-1}))dx &= \int_{\partial K} \vitesse_t (P^n, S^{n-1}) \cdot n^K d\sigma 
	\end{align*}
	
	où $\partial K$ représente le bord de la cellule K. Donc nous pouvons discrétiser $\partial K$ par les 4 arrêtes de $\epsilon_K$. Ainsi, 
	
	\begin{align*}
		\int_K \divergence(\vitesse_t (P^n, S^{n-1}))dx &= \int_{\partial K} \vitesse_t (P^n, S^{n-1}) \cdot n^K d\sigma \\
			&= \sum_{\sigma \in \epsilon_K} \int_{\sigma} \vitesse_t (P^n, S^{n-1}) \cdot n^K_\sigma d\sigma
	\end{align*}
	
	\medskip
	Nous pouvons alors approximer $\int_{\sigma} \vitesse_t (P^n, S^{n-1}) \cdot n^K_\sigma d\sigma$ par $F_{T, \sigma} (P_K^{n}, P_L^n)$
	
	\begin{equation}
		\label{defflux}
		\left\{
			\begin{array}{ll}
				F_{T, \sigma}^{int} (P_K^n, P_L^n) = M_{T, \sigma}^{int} \cdot T_\sigma^{int} (P^n_K - P^n_L) \\
				F_{T, \sigma}^{bord} (P_K^n, P_\sigma) = M^{bord}_{T, \sigma} \cdot T_\sigma^{bord} (P_K^{n} -  P_\sigma)
			\end{array}
		\right.
	\end{equation}
	
	où :
	
	\begin{itemize}
		\item $F_{T, \sigma} (P_K^n, P_L^n)$ est le flux entre $P_K^n$ et $P_L^n$
		\item $M_{T, \sigma}$ est la mobilité totale
		\item $T_\sigma$ est la transmissivité
	\end{itemize}
	
	
	
	% ---------------- TRANSMISSIVITES DES ARRETES ---------------
	\subsection{Calcul des transmissivités sur les arrêtes}
		Commençons par déterminer les transmissivités des différentes arrêtes:
		
		\medskip
		Si $\sigma \in \epsilon_K \cap \epsilon_{int} $ et $\sigma = \partial K \cap \partial L$
		
		alors nous allons utilisé la moyenne harmonique pour calculer la transmissivité entre les deux mailles K et L:
		
		\begin{align*}
			\frac{1}{T_\sigma^{int}} 
				&= \frac{d_{K_\sigma}}{\overline{K}_K |\sigma|} 
					+ \frac{d_{L_\sigma}}{\overline{K}_L |\sigma|}\\
				&= \frac{d_{K_\sigma}\overline{K}_L}{\overline{K}_K |\sigma| \overline{K}_L} 
					+ \frac{d_{L_\sigma}\overline{K}_K}{\overline{K}_L |\sigma| \overline{K}_K} \\
				&= \frac{d_{K_\sigma}\overline{K}_L + d_{L_\sigma}\overline{K}_K}{\overline{K}_L |\sigma| \overline{K}_K}
		\end{align*}
		
		d'où  $ T_\sigma^{int} = |\sigma| \frac{ \overline{K}_K \overline{K}_L }{ d_{K_\sigma}\overline{K}_L + d_{L_\sigma}\overline{K}_K } $
		
		\medskip
		De la même façon, en considérant $\sigma \in \epsilon_K \cap \epsilon_{bord} $, on a :
		
		\[
			T_\sigma^{bord} = |\sigma| \frac{\overline{K}_K}{d_{K_\sigma}}
		\]
		
	% ---------------- CALCUL DES MOBILITES TOTALES  ---------------
	\subsection{Calcul des mobilités totales}
	
	De la même manière que pour le calcul des transmissivités, nous séparons le calcul des mobilités totales internes du calcul des mobilités totales de bord:
	
	\begin{itemize}
		\item si $\sigma \in \epsilon_K \cap \epsilon_{int} $ et $\sigma = \partial K \cap \partial L$ \\
			On calcule la mobilité totale à l'aide du gradient de Pression : si $P_K^n \geq P_L^n$ alors cela signifie que le flux se déplace de la cellule K vers la cellule L; à l'inverse, si $P_K^n < P_L^n$. On retrouve donc l'équation suivante:
			
			\begin{equation}
				M_{T, \sigma}^{int} = 
				\left\{
					\begin{array}{ll}
						M_t(S^n_K) \quad si \; 	P_K^n \geq P_L^n\\
						M_t(S^n_L) \quad sinon					
					\end{array}					
				\right.
			\end{equation}
			
			\medskip
			Dans le cas de la mobilité initiale: n=0, nous utilisons la formule explicite présentée dans la section 1, qui nous dit que :
			\[
				M_t(S) = M_w(1 - S) + M_o (S)
			\]
			et considérant $S = S_{init}$ la saturation initiale donnée.
		
		 \item $\sigma \in \epsilon_K \cap \epsilon_{bord} $, on effectue le même raisonnement que dans le cas des arrêtes internes mais en considérant $P_\sigma$ à la place de la $P_L$ la pression de la cellule L (qui n'existe pas ici). Ainsi:
		 
		\begin{equation}
			M_{T, \sigma}^{int} = 
			\left\{
				\begin{array}{lll}
					M_t(S^n_K) \quad si \; 	P_K^n \geq P_\sigma \quad et \; sur \; [\frac{Ly}{2}+ 1; Ly] \\
					M_t(S_\sigma) \quad si \; P_\sigma > P_K^n \; et \; sur \; [\frac{Ly}{2}+ 1; Ly]				
				\end{array}					
			\right.
		\end{equation} 
		
		\medskip
		Etudions plus particulièrement le cas n=0 pour la mobilité totale au bord:
		\begin{itemize}
			\item \underline{Le bord gauche:} On injecte uniquement de l'eau sur le bord gauche, aussi la mobilité totale équivaut à la mobilité de l'eau. On a donc:
		
		\[
			M_t(S_{init}) = M_w(1 - {S_init})
		\]
		
			\item \underline{Le bord droit:} Il y a une interaction entre l'huile qui sort et l'eau présent à la sortie. Nous utilisons donc la même méthode de calcul que pour la mobilité totale intérieure:
			
			
		\begin{equation*}
			\left\{
				\begin{array}{ll}
					M_t(S_{init}) = M_w(1 - S_{init}) + M_o (S_init) \quad sur \; [\frac{Ly}{2}+ 1; Ly] \\
					M_t(S_init) = 0 \quad sinon
				\end{array}
			\right.
		\end{equation*}
		
		Le deuxième terme provient de la condition limite donnée dans l'énoncé.
		\end{itemize}
			
	\end{itemize}. 
	
	
	
	% ---------------- EQUATION DE LA PRESSION ---------------
	\subsection{Equation discrète de la pression}
	Après avoir explicité l'ensemble des termes présentés dans l'approche discrète de  l'intégration de $\int_K \divergence (\vitesse_t(P^{n+1}, S^n))dx$, nous pouvons en déduire l'équation discrète de la pression:
	
	\begin{align*}
		\int_\Omega \divergence (\vitesse_t(P^{n+1}, S^n))dx 
			&= \sum_{K \in \mathcal{M_h}} \int_K \divergence (\vitesse_t(P^{n+1}, S^n))dx \\
			&= \sum_{K \in \mathcal{M_h}} \int_{\partial K} \vitesse_t(P^{n+1}, S^n)\cdot n d\sigma \\
			&= \sum_{\sigma \in \partial K \cap \partial L} F_{T, \sigma}^{int} (P^{n+1}_K, P^{n+1}_L) + \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} F_{T, \sigma}^{bord} (P^{n+1}_K, P_\sigma)
	\end{align*}
	
	
	
	
	
	% ---------------- EQUATION DE LA SATURATION ---------------
	\subsection{Equation discrète de la saturation}
	
	Afin de déterminer l'équation discrète de la saturation, nous nous intéressons à la seconde équation de (\ref{sytemModifie}).
	
	Nous rappelons que nous pouvons approximer $\partial_t S$ par $\frac{S^{n+1} - S^n}{\Delta t}$, $\forall n \in \{0, ..., N_t \}$. Alors en intégrant sur une cellule K du maillage, on obtient:
	
	\begin{align*}
		& \int_K \phi \frac{S^{n+1} - S^n}{\Delta t} + \divergence (\frac{M_o(S)}{M_t(S)}\vitesse_t(S))dx \\
			&= \int_K \phi \frac{S^{n+1} - S^n}{\Delta t}dx + \int_K \divergence (\frac{M_o(S)}{M_t(S)}\vitesse_t(S))dx  \\
			&= |K|  \phi \frac{S^{n+1} - S^n}{\Delta t} + \int_{\partial K} \frac{M_o(S)}{M_t(S)}\vitesse_t(S) \cdot n d\sigma \\
			&= |K|  \phi \frac{S^{n+1} - S^n}{\Delta t} + \sum_{\sigma \in \epsilon_K \cap \epsilon_{int}} \int_\sigma \frac{M_o(S)}{M_t(S)}\vitesse_t(S) \cdot n d\sigma + \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} \int_\sigma \frac{M_o(S)}{M_t(S)}\vitesse_t(S) \cdot n d\sigma
	\end{align*}

	
	\medskip
	Et on dit que :
	
	\begin{align}
		\sum_{\sigma \in \epsilon_K \cap \epsilon_{int}} \int_\sigma \frac{M_o(S)}{M_t(S)}\vitesse_t(S) \cdot n d\sigma \approx \sum_{\sigma \in \epsilon_K \cap \epsilon_{int}} F_{o, \sigma}^{int}(S^n_K, S^n_L) \\
		\sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} \int_\sigma \frac{M_o(S)}{M_t(S)}\vitesse_t(S) \cdot n d\sigma \approx \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} F_{o, \sigma}^{bord}(S^n_K)
	\end{align}
	
	avec $  F_{o, \sigma}^{int}(S^n_K, S^n_L)  $ et $F_{o, \sigma}^{bord}(S^n_K)$ les termes de flux discrétisés sur les arrêtes internes et de bord.
	
	\begin{equation*}
		\left\{
			\begin{array}{ll}
				F_{o, \sigma}^{int} (S^n_K, S^n_L) = \frac{M_o(S^n_K)}{M_t(S^n_K)} (F_{t, \sigma, KL}^{int, n+1})^{+} + \frac{M_o(S^n_L)}{M_t(S^n_L)} (F_{t, \sigma, KL}^{int, n+1})^{-} \\
				F_{o, \sigma}^{bord}(S^n_K)= \frac{M_o(S^n_K)}{M_t(S^n_K)} (F_{t, \sigma}^{bord,, n+1})^{+} + \frac{M_o(S_\sigma)}{M_t(S_\sigma)} (F_{t, \sigma}^{bord, n+1})^{-} 
			\end{array}			
		\right.
	\end{equation*}
	
	
	\bigskip
	On a finalement l'équation discrète de la saturation: 
	
	\[
		|K|  \phi \frac{S^{n+1} - S^n}{\Delta t} + 
		\sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} F_{o, \sigma}^{int} (S^n_K, S^n_L)
		+ \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} F_{o, \sigma}^{bord}(S^n_K) = 0
	\]
	
	d'où 
	
	\begin{equation}
		\label{equSaturation}
		S^{n+1}_K = S^n_K - \frac{\Delta t}{\phi |K|} \left(  \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} F_{o, \sigma}^{int} (S^n_K, S^n_L)
		+ \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} F_{o, \sigma}^{bord}(S^n_K) \right) =0
	\end{equation}
	
	\medskip
	\textit{ On remarque que cette équation est bien explicite}
	
	
	% ---------------- CALCUL DE LA CFL ---------------
	\subsection{Détermination de la CFL du schéma IMPES}
	
	Nous allons déterminer la condition CFL du schéma en se basant sur l'équation discrète de la saturation (\ref{equSaturation}) et en rappelant que :
	
	\[
		- \left(  \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} F_{o, \sigma, KL}^{int, n+1} + \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} F_{o, \sigma}^{bord, n+1}  \right) \times \frac{M_o(S^n_K)}{M_t(S^n_K)} = 0
	\]
	
	d'où on en déduit:
	
	\begin{multline*}
		|K|  \phi \frac{S^{n+1} - S^n}{\Delta t} + 
		\sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} F_{o, \sigma}^{int} (S^n_K, S^n_L)
		+ \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} F_{o, \sigma}^{bord}(S^n_K) \\
		 - \left(  \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} F_{o, \sigma, KL}^{int, n+1} + \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} F_{o, \sigma}^{bord, n+1}  \right) \times \frac{M_o(S^n_K)}{M_t(S^n_K)}  = 0
	\end{multline*}
	
	\begin{multline*}
		\Longleftrightarrow  |K|  \phi \frac{S^{n+1} - S^n}{\Delta t} 
		+ \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} \frac{M_o(S^n_K)}{M_t(S^n_K)} (F_{t, \sigma, KL}^{int, n+1})^{+} + \frac{M_o(S^n_L)}{M_t(S^n_L)} (F_{t, \sigma, KL}^{int, n+1})^{-}
		+ \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} \frac{M_o(S^n_K)}{M_t(S^n_K)} (F_{t, \sigma}^{bord, n+1})^{+} + \frac{M_o(S_\sigma)}{M_t(S_\sigma)} (F_{t, \sigma}^{bord, n+1})^{-}  \\
		 - \left(  \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} F_{o, \sigma, KL}^{int, n+1} + \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} F_{o, \sigma}^{bord, n+1}  \right) \times \frac{M_o(S^n_K)}{M_t(S^n_K)}  = 0
	\end{multline*}
	
	et on rappelle que :
	
	\begin{align*}
		\sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} F_{o, \sigma, KL}^{int, n+1} = \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{+} + \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{-} \\
		\sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} F_{o, \sigma}^{bord, n+1} =  \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{+} +  \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{-}
	\end{align*}	
	
	alors on a:
	
	\begin{multline*}
		\Longleftrightarrow  |K|  \phi \frac{S^{n+1} - S^n}{\Delta t} \\
		+ \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} \frac{M_o(S^n_K)}{M_t(S^n_K)} (F_{t, \sigma, KL}^{int, n+1})^{+} + \frac{M_o(S^n_L)}{M_t(S^n_L)} (F_{t, \sigma, KL}^{int, n+1})^{-}
		+ \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} \frac{M_o(S^n_K)}{M_t(S^n_K)} (F_{t, \sigma}^{bord, n+1})^{+} + \frac{M_o(S_\sigma)}{M_t(S_\sigma)} (F_{t, \sigma}^{bord, n+1})^{-}  \\
		 - \left(  \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{+} + \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{-} + \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{+} +  \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{-}  \right) \times \frac{M_o(S^n_K)}{M_t(S^n_K)}  = 0
	\end{multline*}
	
	\begin{multline*}
		\Longleftrightarrow |K|  \phi \frac{S^{n+1} - S^n}{\Delta t} 
		+ \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{-} \left( \frac{M_o(S^n_K)}{M_t(S^n_K)} - \frac{M_o(S^n_L)}{M_t(S^n_L)} \right) \\
		+   \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{-} \left( \frac{M_o(S^n_K)}{M_t(S^n_K)} - \frac{M_o(S^n_L)}{M_t(S^n_L)} \right) = 0
	\end{multline*}
	
	\begin{multline}
		\label{calcCFL}
		\Longleftrightarrow |K|  \phi \frac{S^{n+1} - S^n}{\Delta t} 
		+ \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{-} \frac{\left( \frac{M_o(S^n_K)}{M_t(S^n_K)} - \frac{M_o(S^n_L)}{M_t(S^n_L)} \right) }{S^n_K - S^n_L} (S^n_K - S^n_L) \\
		+   \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{-} \frac{ \left( \frac{M_o(S^n_K)}{M_t(S^n_K)} - \frac{M_o(S^n_L)}{M_t(S^n_L)} \right) }{S^n_K - S^n_L} (S^n_K - S^n_L) = 0
	\end{multline}
	
	
	En utilisant le théorème des accroissements finis on peut écrire:
	\begin{equation*}
		 \left\{
		 	\begin{array}{ll}
		 		\frac{\left( \frac{M_o(S^n_K)}{M_t(S^n_K)} - \frac{M_o(S^n_L)}{M_t(S^n_L)} \right) }{S^n_K - S^n_L} = \delta_{KL} \\
		 		 \frac{ \left( \frac{M_o(S^n_K)}{M_t(S^n_K)} - \frac{M_o(S^n_L)}{M_t(S^n_L)} \right) }{S^n_K - S^n_L} =  \delta_{K\sigma}
		 	\end{array}
		 \right.
	\end{equation*}
	
	\bigskip
	Nous pouvons écrire l'équation (\ref{calcCFL}) de façon à exprimer $S^{n+1}_K$: 
	
	\begin{multline*}
		S^{n+1}_K = [ 1 + \frac{\Delta t}{\phi |K|} \left(  
			\sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{-} \delta_{KL} 
		 +  \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{-} \delta_{K\sigma}	
		 \right) ] S_K \\
		- \left( \frac{\Delta t}{\phi |K|} \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{-} \delta_{KL} S^n_L \right) - \left( \frac{\Delta t}{\phi |K|}  \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{-} \delta_{K\sigma} S_\sigma \right)
	\end{multline*}
		

	\medskip
	Nous cherchons la condition de stabilité du schéma c'est à dire une condition sur  $\Delta t$ tel que  $ |S^{n+1} - S^n| < 1, \forall K \in \mathcal{M_h}  $:
	
	Alors:

	\begin{equation*}
		| S^{n+1}_K - S^n_K | < 1
	\end{equation*}
	
	\begin{multline*}
		\Longleftrightarrow | [ 1 + \frac{\Delta t}{\phi |K|} \left(  
			\sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{-} \delta_{KL} 
		 +  \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{-} \delta_{K\sigma}	
		 \right) ] S_K \\
		- \left( \frac{\Delta t}{\phi |K|} \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{-} \delta_{KL} S^n_L \right) - \left( \frac{\Delta t}{\phi |K|}  \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{-} \delta_{K\sigma} S_\sigma \right) - S^n_K | < 1
	\end{multline*}
	
	\begin{multline*}
		\Longleftrightarrow | \frac{\Delta t}{\phi |K|} \left(  
			\sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{-} \delta_{KL} 
		 +  \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{-} \delta_{K\sigma}	
		 \right) S_K \\
		- \left( \frac{\Delta t}{\phi |K|} \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{-} \delta_{KL} S^n_L \right) - \left( \frac{\Delta t}{\phi |K|}  \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{-} \delta_{K\sigma} S_\sigma \right) | < 1
	\end{multline*}
	
	\begin{align*}
		\Longleftrightarrow \Delta t < |  \frac{\phi |K|}{ \sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{-} \delta_{KL} (S^n_K - S^n_L)  + \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{-} \delta_{K\sigma} (S^n_K - S_\sigma) } | \\
		\Longleftrightarrow \Delta t < |  \frac{\phi |K|}{ \left(\sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{-} (S^n_K - S^n_L)  + \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{-} (S^n_K - S_\sigma) \right) \delta} | 
	\end{align*}

	où $\delta= sup_{K \in \mathcal{M_h}} (\delta_{KL}, \delta_{K\sigma})$

	De même on peut écrire 
	
	\begin{equation}
		\label{CFL}
		\Delta t < |  \frac{\phi |K|}{ 
			sup_{K \in \mathcal{M_h}} 
			\left(
			\sum_{\sigma \in \partial K \cap \partial L \in \epsilon_K  \cap \epsilon_{int}} (F_{o, \sigma, KL}^{int, n+1})^{-} 
			+ \sum_{\sigma \in \epsilon_K \cap \epsilon_{bord}} (F_{o, \sigma}^{bord, n+1})^{-}
			 \right) \delta
			 } | 
	\end{equation}

	et nous pouvons définir $\delta \leq f_o'(S)$ que nous utiliserons dans la code numérique.




\end{document}



























