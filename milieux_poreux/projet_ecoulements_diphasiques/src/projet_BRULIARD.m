%% COURS PROJET: MILIEUX POREUX - MACS 2 
% 
% author: Margaux BRULIARD
% date: 20.06.2018
%
% Simulation des ecoulements diphasiques icompressibles en milieux poreux



clear all

% ------------------------------------------------
%       COMMANDES D'AFFICHAGE DES GRAPHIQUES
% 
%   1 pour oui - 0 pour non
%------------------------------------------------

affichage_pression = 0 ;
affichage_saturation = 0 ;
affichage_CFL = 1;



% ------------------------------------------------
%       JEUX DE DONNEES
% -------------------------------------------------
Lx = 1000 % longueur du domaine en x
Ly = 1000 % longueur du domaine en y


Nx = 20 ;
Ny = 20 ; 
N = Nx*Ny ;
Nint = Ny*(Nx-1) + Nx*(Ny-1); % nombre d'aretes internes
Nbord = 2*Ny; % nombre d'arretes de bord


phi = 0.1 % porosite


Swi = 0.1;
Sor = 0.3;

muw = 1e-3 ;
muo = 5*10^(-3) ;

P_in = 2*10^(7);
P_out = 9*10^(6) ;

S_init = 1 - Swi ;

temps_final = 60*365*24*3600 ; % temps final 60 ans converti en secondes
Deltat = 1e7 ;


krw = @(sw) (  ((sw - Swi)/(1-Swi))^2  ) ;
kro = @(so) (  ((so - Sor)/(1-Sor))^2  ) ;


K1 = 10^(-13) ;
K2 = K1/100 ;


% ------------------------------------------------
%       CREATION DU MAILLAGE
% -------------------------------------------------
dx = Lx / Nx ;
dy = Ly / Ny ;


for i=1:Nx
    Xi(i) = (i-1/2)*dx; % Xi et Yi sont les centres des mailles
    for j=1:Ny  
      Yj(j) = (j-1/2)*dy;

      m = i + (j-1)*Nx; % numero de la maille

      volume(m) = dx*dy;
      X(m) = Xi(i);
      Y(m) = Yj(j);
    end
end


% ------------------------------------------------
%       CONSTRUCTION DES ARETES 
% -------------------------------------------------

% -------------------- aretes interieures -------------------- %
isig = 0;

% ---- aretes internes verticales ---- %  
for i=1:Nx-1
    for j=1:Ny
      
      m1 = i+(j-1)*Nx;
      m2 = i+1+(j-1)*Nx;
      
      isig = isig + 1;
      
      % stockage des infos sur les mailles internes
      mailleint(isig,1) = m1;
      mailleint(isig,2) = m2;
      
      surfaceint(isig) = dy;  
      
      Xint(isig) = (X(m1)+X(m2))/2;
      Yint(isig) = Y(m1);
    end
end 


% ---- aretes internes horizontales ---- %
for i=1:Nx
    for j=1:Ny-1
      m1 = i+(j-1)*Nx;
      m2 = i+ j*Nx;
      isig = isig + 1;
      
      % stockage des infos sur les mailles internes
      mailleint(isig,1) = m1;
      mailleint(isig,2) = m2;
      surfaceint(isig) = dx;
      Xint(isig) = X(m1);
      Yint(isig) = (Y(m1)+Y(m2))/2;
    end
end

% -------------------- aretes de bord -------------------- %
isig = 0;

% ---- aretes bord gauche ---- %
i=1;
for j=1:Ny
  m1 = i+(j-1)*Nx;
  isig = isig + 1;
  
  
  % infos sur les mailles de bord
  maillebord(isig) = m1;
  surfacebord(isig) = dy;  
  
  
  Xbord(isig) = 0;
  Ybord(isig) = Y(m1);
end 


% -----  aretes bord droit ---- %
i=Nx;
for j=1:Ny
  m1 = i+(j-1)*Nx;
  isig = isig + 1;
  
  % infos sur les mailles de bord
  maillebord(isig) = m1;
  surfacebord(isig) = dy;  
  
  Xbord(isig) = Lx;
  Ybord(isig) = Y(m1);
end


% ------------------------------------------------
%       PERMEABILITE DES MAILLES
% ------------------------------------------------

% -- par defaut on defini la permeabilite K1 dans toutes les cellules
for m=1:N
    perm(m) = K1;
end

% -- construction première colonne sur [100:200] x [0:500]
x_start = 1 + 100/(Lx/Nx); 
x_end = x_start + 2;

y_start = 1; 
y_end = 1 + 500/(Ly/Ny) ;

for i=x_start:x_end 
    for j=y_start:y_end
        m = i + (j-1)*Nx ;
        
        perm(m) = K2 ;
    end
end


% -- construction deuxieme colonne [300:500] x [300 x 1000]
x_start = 1 + 300/(Lx/Nx); 
x_end = 1 + 500/(Lx/Nx);

y_start = 1 + 300/(Ly/Ny); 
y_end = 1 + 1000/(Ly/Ny) ;

for i=x_start:x_end 
    for j=y_start:y_end
        m = i + (j-1)*Nx ;
        
        perm(m) = K2 ;
    end
end


% -- construction troisieme colonne [600:800]x [0:500]
x_start = 1 + 600/(Lx/Nx); 
x_end = 1 + 800/(Lx/Nx);

y_start = 1; 
y_end = 1 + 500/(Ly/Ny) ;

for i=x_start:x_end 
    for j=y_start:y_end
        m = i + (j-1)*Nx ;
        
        perm(m) = K2 ;
    end
end



% -----------------------------------------------
%       TRANSMISSIVITES SUR LES ARRETES
% -----------------------------------------------

% ---- transmissivite des arretes interieures ---- %
for isig=1:Nint
  m1 = mailleint(isig,1);
  m2 = mailleint(isig,2);
  
  d1_sigma = sqrt((X(m1)-Xint(isig))^2 + (Y(m1)-Yint(isig))^2);
  d2_sigma = sqrt((X(m2)-Xint(isig))^2 + (Y(m2)-Yint(isig))^2);

  % calcul des transmissivites par moyenne harmonique
  Tint(isig) = perm(m1)*perm(m2) * surfaceint(isig) ...
      / (d2_sigma*perm(m1) + d1_sigma*perm(m2)) ; 
end


% ---- transmissivite des arretes de bord
for isig=1:Nbord
  m = maillebord(isig);
  d_sigma = sqrt((X(m)-Xbord(isig))^2 + (Y(m)-Ybord(isig))^2);
  Tbord(isig) = perm(m) * surfaceint(isig) / d_sigma ; 
end




% ------------------------------------------------
%       PRESSION ET SATURATION INTIALES
% -------------------------------------------------

% ---- Calcul de la pression au bord ---- %
Pbord=ones(Nbord,1);

% - Pbord a gauche
Pbord(1:Nbord/2)=P_in;

% - Pbord a droite
Pbord(Nbord/2+1:3*Nbord/4) = P_out ;
Pbord (3*Nbord/4+1:Nbord) = P_out;


% ---- Calcul de la saturation initiale ---- %
S_old = S_init*(ones(N, 1));
S_sigma_in = 1.; % condition de saturation au bord gauche
S_sigma_out = 0.; % condition de saturation au bord droit


% ------------------------------------------------------ 
%          CALCUL DE LA MOBILITE TOTALE t_0
% ------------------------------------------------------

% ---- calcul de la mobilité totale interne Mtint ---- % 
for isig = 1:Nint
    Mtint(isig) = Mw(1- S_init, Swi, muw) + Mo(S_init, Sor, muo);
end

% ---- calcul de la mobilite totale de bord Mtbord ---- %
Mtbord(1:Nbord/2) = Mw(1.0 , Swi, muw); % arrete de bord gauche (on injecte que de l'eau)
Mtbord(Nbord/2+1:Nbord) = Mw(1- S_init, Swi, muw) + Mo(S_init, Sor, muo);  % arrete de bord droit (ce qui est dans le reservoir sort)



% ------------------------------------------------------ 
%          BOUCLE DE TEMPS t_n
% ------------------------------------------------------
temps = 0;
ind_CFL = 1;
while temps <= temps_final
    
    % ---- Calcul de la pression P ---- %
    A=sparse(N,N);
    B=sparse(N,1);
    
    % -- arretes interieures
    for isig=1:Nint
        m1=mailleint(isig,1);
        m2=mailleint(isig,2);

        coeff=Mtint(isig)*Tint(isig);

        A(m1,m1)=A(m1,m1)+coeff;
        A(m1,m2)=A(m1,m2)-coeff;
        A(m2,m1)=A(m2,m1)-coeff;
        A(m2,m2)=A(m2,m2)+coeff;
    end

    % -- arretes de bord
    for isig=1:Nbord
        m=maillebord(isig);

        coeff=Mtbord(isig)*Tbord(isig);

        A(m,m)=A(m,m)+coeff;
        B(m)=B(m)+coeff*Pbord(isig);
    end
    
    % -- resolution systeme
    P=A\B;
    
    
    if (affichage_pression == 1)
        gridX = reshape(X, Ny, Nx);
        gridY = reshape(Y, Ny, Nx);
        gridP = reshape(P, Ny, Nx);
        surf(gridX, gridY, gridP);
        xlabel ('Lx [m]') ;
        ylabel('Ly [m]');
        zlabel('Pression [Pa]');
        title(sprintf('Pression \nt=%d', temps));
        pause(0.01);
    end

    
    
    % ---- Calcul des flux ---- %
    
    % -- arretes internes
    for isig=1:Nint
        m1 = mailleint(isig,1);
        m2 = mailleint(isig,2);

        Ftint(isig) = Mtint(isig)*Tint(isig)*( P(m1) - P(m2));
    end
    
    
    
    % -- aretes de bord
    Ftbord = zeros(1, Nbord);
    for isig = 1:Nbord/2
        m=maillebord(isig);
        
        Ftbord(isig) = Mtbord(isig)*Tbord(isig)*(P(m) - Pbord(isig));
    end
    
    for isig = 3*Nbord/4+1:Nbord
        m=maillebord(isig);
        
        Ftbord(isig) = Mtbord(isig)*Tbord(isig)*(P(m) - Pbord(isig));
    end
    
    
    
    
    
    
    % ---- Calcul de la CFL ---- %
%     max(S_old)
%     Fop(muo, muw, Sor, Swi, max(S_old))
    supFop = 1 ; %ax ( Fop(muo,muw,Sor,Swi,S_old)  ) 
    
    infK = volume(1) ;  % le maillage est régulier
    
    
    sumFm = zeros(1, N) ;
    
    for isig = 1:Nbord
        m = maillebord(isig);
        
        sumFm(m) = sumFm(m) - min (Ftbord(isig), 0) ;
    end
    
    for isig = 1:Nint 
        m1 = mailleint(isig, 1) ;
        m2 = mailleint (isig, 2) ;
        
        sumFm(m1) = sumFm(m1) - min(Ftint(isig), 0);
        sumFm(m2) = sumFm(m2) - min(Ftint(isig), 0) ;
    end
    
    supFm = max ( sumFm);
    
    Deltat = phi*infK/ (supFm * supFop) ;
    condCFL(ind_CFL) = Deltat ;
    ind_CFL = ind_CFL +1 ;
    

    
    % ---- Calcul de la Saturation ---- %
    Saturation = S_old; 
    
    for isig=1:Nint
        m1 = mailleint(isig, 1);
        m2 = mailleint(isig, 2);
        

        terme1 = Fo( muo, muw, Sor, Swi, S_old(m1)) * max( Ftint(isig), 0);
        terme2 = Fo(muo, muw, Sor, Swi, S_old(m2))* min(Ftint(isig), 0);
        
        Saturation(m1) = Saturation(m1) - Deltat/(volume(m1)*phi)*(terme1 + terme2);        
        Saturation(m2) = Saturation(m2) + Deltat/(volume(m2)*phi)*(terme1 + terme2);
    end
    
    
    % -- saturation au bord gauche: Ssigma = 0;
    for isig=1:Nbord/2
        m = maillebord(isig);
        
        terme = Fo( muo, muw, Sor, Swi, S_old(m)) * max(Ftbord(isig), 0) + ...
                Fo( muo, muw, Sor, Swi, 0) * min( Ftbord(isig), 0);        
        Saturation(m) = Saturation(m) - Deltat/(volume(m)*phi)* terme; 
    end
    
    % -- saturation au bord droit: Ssigma = 1;
    for isig=Nbord/2+1:Nbord
        m = maillebord(isig);
        
        terme = Fo( muo, muw, Sor, Swi, S_old(m)) * max(Ftbord(isig), 0) + ...
                Fo( muo, muw, Sor, Swi, 1) * min( Ftbord(isig), 0);        
       Saturation(m) = Saturation(m) - Deltat/(volume(m)*phi)* terme;
    end
   
    
    % -- incrémentation saturation
    S_old = Saturation;
    
    
    % -- Affichage saturation
    if (affichage_saturation == 1)
        gridX = reshape(X, Ny, Nx);
        gridY = reshape(Y, Ny, Nx);
        gridS = reshape(Saturation, Ny, Nx);
        surf(gridX, gridY, gridS);
        xlabel('Lx [m]');
        ylabel('Ly [m]');
        zlabel('Saturation [-]');
        zlim([0, S_init]);
        title(sprintf('Saturation \nt=%d', temps));
        pause(0.01)
    end
    
    
    % ----  Mise a jour de la mobilite totale ---- %
    for isig=1:Nint
        m1 = mailleint(isig, 1);
        m2 = mailleint(isig, 2);

        if (P(m1) >= P(m2))
            Mtint(isig) = Mw(1- Saturation(m1), Swi, muw) + Mo(Saturation(m1), Sor, muo);
        else
            Mtint(isig) = Mw(1- Saturation(m2), Swi, muw) + Mo(Saturation(m2), Sor, muo);
        end
    end

    for isig=1:Nbord/2
        m = maillebord(isig);

        Mtbord(isig) = Mw (1.0, Swi, muw); 
    end

    for isig=Nbord/2+1:Nbord
        m = maillebord(isig);

        Mtbord(isig) = Mw(1- Saturation(m), Swi, muw) + Mo(Saturation(m), Sor, muo);
    end
    
    
    
    % ---- Calcul des debit instantanes et cumules d'hulie et d'eau à la
    % sortie du bord droit ---- %
    
    
    
    
    
    
    % ---- Mise a jour de temps ---- %
    temps = temps + Deltat ;
    
    
end


if (affichage_CFL == 1)
   
    plot(condCFL);
    title ('Evolution de la CFL au cours du temps');
    xlabel('instant $t^n$');
    ylabel('CFL = Deltat');
    
    
end





