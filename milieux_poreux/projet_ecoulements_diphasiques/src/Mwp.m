
function dMwdSw = Mwp(Sw,Swi,muw)
    % Calcul de la dérivée de la mobilité de la phase eau : 
    %
    %           dMwdSw = dkrwdSw(Sw)/muw
    %
    % où dKrwdSw(Sw) est la dérivée de la permeabilité relative de l'eau : 
    %
    %           dkrwdSw(Sw) = 2*(sw - swi) / (1-swi)^2
    %
    % Donnees d'entrees
    % Sw    : saturation de la phase eau [-]
    % Swi   : saturation irréductible de la phase eau [-]
    % muw   : viscosité de la phase eau[Pa/s]
    %
    % Donnees de sortie :
    % dMwdSw : dérivée de la mobilité de l'eau  [s/Pa]
 if (Sw<Swi) 
  dMwdSw = 0;
 else 
  dMwdSw = 2*(Sw-Swi)/(1-Swi)^2/muw;
 end
end