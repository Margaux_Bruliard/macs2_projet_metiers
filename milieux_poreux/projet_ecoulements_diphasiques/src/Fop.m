function dFodSo = Fop(muo,muw,Sor,Swi,So)
    % Calcul de la dérivée du flux fractionnaire : 
    %
    %           dFodSo(so)= Mo(So)/(Mo(So)+Mw(1-So))
    %
    % Donnees d'entrees
    % muo   : viscosité de la phase huile [Pa/s]
    % muw   : viscosité de la phase eau [Pa/s]
    % Sor   : saturation résiduelle de la phase huile [-]
    % Swi   : saturation irréductible de la phase eau [-]
    % So    : saturation de la phase huile [-]
    %
    % Donnees de sortie :
    % dFodSo : dérivée du flux fractionnaire [-]
    %
    % 
    dFodSo = (Mop(So,Sor,muo).*Mw(1-So,Swi,muw) ...
        + Mo(So,Sor,muo).*Mwp(1-So,Swi,muw))./(Mo(So,Sor,muo) ...
        + Mw(1-So,Swi,muw)).^2;
end 