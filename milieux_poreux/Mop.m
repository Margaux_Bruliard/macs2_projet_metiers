
function  dModSo= Mop(So,Sor,muo)
    % Calcul de la dérivée de la mobilité de la phase huile : 
    %
    %           dModSo = dkrodSo(So)/muo
    %
    % où dKrodSo(So) est la dérivée de la permeabilité relative de l'huile : 
    %
    %           dkrodSo(So) = 2*(so - sor) / (1-sor)^2
    %
    % Donnees d'entrees
    % So    : saturation de la phase huile [-]
    % Sor   : saturation résiduelle de la phase huile [-]
    % muo   : viscosité de la phase huile [Pa/s]
    %
    % Donnees de sortie :
    % dModSo : dérivée de la mobilité de l'huile  [s/Pa]
    %
    % 
  if (So<Sor)
   dModSo = 0;
  else 
   dModSo = 2*(So-Sor)/(1-Sor)^2/muo;
  end  
end