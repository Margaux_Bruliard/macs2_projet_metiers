function fracflow = Fo(muo,muw,Sor,Swi,So)
    % Calcul du flux fractionnaire : 
    %
    %           Fo(so)= Mo(So)/(Mo(So)+Mw(1-So))
    %
    % Donnees d'entrees
    % muo   : viscosité de la phase huile [Pa/s]
    % muw   : viscosité de la phase eau [Pa/s]
    % Sor   : saturation résiduelle de la phase huile [-]
    % Swi   : saturation irréductible de la phase eau [-]
    % So    : saturation de la phase huile [-]
    %
    % Donnees de sortie :
    % fracflow : flux fractionnaire [-]
    fracflow = Mo(So,Sor,muo)/(Mo(So,Sor,muo) + Mw(1-So,Swi,muw));
end