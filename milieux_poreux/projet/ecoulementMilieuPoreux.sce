//-------------------------------------------------------------------------------------
// Simulation 2D horizontale d'un écoulement diphasique eau-huile incompressible
// et immiscible.
// Exemple d'un reservoir rectangulaire homogène saturé initalement d'huile.
// On injecte de l'eau par son extrémité gauche. 
// Les conditions aux limites sont de type pression imposée sur ses deux bords.
//
// Les équations sont discrétisées par un schéma volumes finis en espace et un 
// schéma IMPES en temps.
// L'équation en pression est résolue implicitement  et l'équation en saturation 
// est résolue explicitement. la stabilité de ce schéma est assurée en respectant 
// la condition CFL sur le pas de temps.
//-------------------------------------------------------------------------------------
clear

// dimensions du domaine [m]
Lx = 1000;
Ly = 1000;
// temps final [s]
tf = 3600*24*360*50;

//pression au bord gauche  [Pa]
P_out = 9.E+6;

//pression  au bord droit  [Pa]
p_in = 1.25E+7;

//viscosite de l'huile [Pa/s]
muo=5E-3;

//viscosite de l'eau [Pa/s]
muw = 1.E-3

//saturation residuelle en huile [-]
Sor = 0.3;

//saturation irreductible en eau [-]
Swi = 0.1;

//permeabilite absolue [m2]
K=1E-13;

//porosite [-]
phi = 0.1;

//discretisation en espace : maillage cartesien uniforme
Nx =21;
Ny = 21;
N = Nx*Ny;
dx = Lx/Nx;
dy = Ly/Ny;
Nint = Ny*(Nx-1) + Nx*(Ny-1) // nombre d'aretes internes
Nbord = Nx+Ny

Ndtmax=10000;

//maillage cartesien uniforme 2D de pas dx dy 
// mailles m = 1:N
for i=1:Nx
  Xi(i) = (i-1/2)*dx;
for j=1:Ny  
  Yj(j) = (j-1/2)*dy;
  
  m = i + (j-1)*Nx;
  
  volume(m) = dx*dy;
  X(m) = Xi(i);
  Y(m) = Yj(j);
end
end

//structure de donnees aretes interieures: isig = 1:Nint
isig = 0;
//aretes internes verticales 
for i=1:Nx-1
for j=1:Ny
  m1 = i+(j-1)*Nx;
  m2 = i+1+(j-1)*Nx;
  isig = isig + 1;
  mailleint(isig,1) = m1;
  mailleint(isig,2) = m2;
  surfaceint(isig) = dy;  
  Xint(isig) = (X(m1)+X(m2))/2;
  Yint(isig) = Y(m1);
end
end 
//aretes internes horizontales
for i=1:Nx
for j=1:Ny-1
  m1 = i+(j-1)*Nx;
  m2 = i+ j*Nx;
  isig = isig + 1;
  mailleint(isig,1) = m1;
  mailleint(isig,2) = m2;
  surfaceint(isig) = dx;
  Xint(isig) = X(m1);
  Yint(isig) = (Y(m1)+Y(m2))/2;
end
end
//
// aretes de bord
//
isig = 0;
// bord gauche
i=1
for j=1:Ny
  m1 = i+(j-1)*Nx;
  isig = isig + 1;
  maillebord(isig) = m1;
  surfacebord(isig) = dy;  
  Xbord(isig) = 0;
  Ybord(isig) = Y(m1);
end 
// bord droit
i=Nx
for j=1:Ny
  m1 = i+(j-1)*Nx;
  isig = isig + 1;
  maillebord(isig) = m1;
  surfacebord(isig) = dy;  
  Xbord(isig) = Lx;
  Ybord(isig) = Y(m1);
end 

// initialisation de la permeabilite dans les mailles
for m=1:N
    perm(m) = K;
end

//transmissivites des aretes interieures 
for isig=1:Nint
  m1 = mailleint(isig,1);
  m2 = mailleint(isig,2);

  // calcul des transmissivites par moyenne harmonique
  Tint(isig) = 0;// A COMPLETER
end

//transmissivites des aretes de bord
for isig=1:Nbord
  m = maillebord(isig);
  Tbord(isig) = 0.0;// A COMPLETER
end















