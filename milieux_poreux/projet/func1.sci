%
% Calcul de la mobilité de la phase huile : 
%
%           Mo = kro(So)/muo
%
% où Kro(So) est la permeabilité relative de l'huile : 
%
%           kro(So) = (so - sor)^2 / (1-sor)^2
%
% Donnees d'entrees
% So    : saturation de la phase huile [-]
% Sor   : saturation résiduelle de la phase huile [-]
% muo   : viscosité de la phase huile [Pa/s]
%
% Donnees de sortie :
% moboil    : mobilité de l'huile  [s/Pa]
%

function moboil = Mo(So,Sor,muo)
  if (So<Sor)
   moboil = 0;
  else 
   moboil = (So-Sor)**2/(1-Sor)**2/muo
  end
endfunction  
%
% Calcul de la mobilité de la phase eau : 
%
%           Mw = krw(Sw)/muw
%
% où Krw(Sw) est la permeabilité relative de l'eau: 
%
%           krw(Sw) = (sw - swi)^2 / (1-swi)^2
%
% Donnees d'entrees
% Sw    : saturation de la phase huile [-]
% Swi   : saturation irréductible de la phase eau [-]
% muw   : viscosité de la phase eau [Pa/s]
%
% Donnees de sortie :
% mobwater  : mobilité de l'eau  [s/Pa]
%  
  
function mobwater = Mw(Sw,Swi,muw)
 if (Sw<Swi) 
  mobwater = 0;
 else 
  mobwater = (Sw-Swi)**2/(1-Swi)**2/muw
 end
endfunction
%
% Calcul de la dérivée de la mobilité de la phase huile : 
%
%           dModSo = dkrodSo(So)/muo
%
% où dKrodSo(So) est la dérivée de la permeabilité relative de l'huile : 
%
%           dkrodSo(So) = 2*(so - sor) / (1-sor)^2
%
% Donnees d'entrees
% So    : saturation de la phase huile [-]
% Sor   : saturation résiduelle de la phase huile [-]
% muo   : viscosité de la phase huile [Pa/s]
%
% Donnees de sortie :
% dModSo : dérivée de la mobilité de l'huile  [s/Pa]
%
% 
function  dModSo= Mop(So,Sor,muo)
  if (So<Sor)
   dModSo = 0;
  else 
   dModSo = 2*(So-Sor)/(1-Sor)**2/muo
  end  
endfunction  
%
% Calcul de la dérivée de la mobilité de la phase eau : 
%
%           dMwdSw = dkrwdSw(Sw)/muw
%
% où dKrwdSw(Sw) est la dérivée de la permeabilité relative de l'eau : 
%
%           dkrwdSw(Sw) = 2*(sw - swi) / (1-swi)^2
%
% Donnees d'entrees
% Sw    : saturation de la phase eau [-]
% Swi   : saturation irréductible de la phase eau [-]
% muw   : viscosité de la phase eau[Pa/s]
%
% Donnees de sortie :
% dMwdSw : dérivée de la mobilité de l'eau  [s/Pa]
%
%   
function dMwdSw = Mwp(Sw,Swi,muw)
 if (Sw<Swi) 
  dMwdSw = 0;
 else 
  dMwdSw = 2*(Sw-Swi)/(1-Swi)**2/muw
 end
endfunction
%
% Calcul du flux fractionnaire : 
%
%           Fo(so)= Mo(So)/(Mo(So)+Mw(1-So))
%
% Donnees d'entrees
% muo   : viscosité de la phase huile [Pa/s]
% muw   : viscosité de la phase eau [Pa/s]
% Sor   : saturation résiduelle de la phase huile [-]
% Swi   : saturation irréductible de la phase eau [-]
% So    : saturation de la phase huile [-]
%
% Donnees de sortie :
% fracflow : flux fractionnaire [-]
%
%   
function fracflow = Fo(muo,muw,Sor,Swi,So)
  fracflow = Mo(So,Sor,muo)/(Mo(So,Sor,muo) + Mw(1-So,Swi,muw));
endfunction  
  
%
% Calcul de la dérivée du flux fractionnaire : 
%
%           dFodSo(so)= Mo(So)/(Mo(So)+Mw(1-So))
%
% Donnees d'entrees
% muo   : viscosité de la phase huile [Pa/s]
% muw   : viscosité de la phase eau [Pa/s]
% Sor   : saturation résiduelle de la phase huile [-]
% Swi   : saturation irréductible de la phase eau [-]
% So    : saturation de la phase huile [-]
%
% Donnees de sortie :
% dFodSo : dérivée du flux fractionnaire [-]
%
%     
  
function dFodSo = Fop(muo,muw,Sor,Swi,So)
  dFodSo = (Mop(So,Sor,muo)*Mw(1-So,Swi,muw) + Mo(So,Sor,muo)*Mwp(1-So,Swi,muw))/(Mo(So,Sor,muo) + Mw(1-So,Swi,muw))**2;
endfunction 

