
  
function mobwater = Mw(Sw,Swi,muw)
    % Calcul de la mobilité de la phase eau : 
    %
    %           Mw = krw(Sw)/muw
    %
    % où Krw(Sw) est la permeabilité relative de l'eau: 
    %
    %           krw(Sw) = (sw - swi)^2 / (1-swi)^2
    %
    % Donnees d'entrees
    % Sw    : saturation de la phase huile [-]
    % Swi   : saturation irréductible de la phase eau [-]
    % muw   : viscosité de la phase eau [Pa/s]
    %
    % Donnees de sortie :
    % mobwater  : mobilité de l'eau  [s/Pa]
    %  
 if (Sw<Swi) 
  mobwater = 0;
 else 
  mobwater = (Sw-Swi)^2/(1-Swi)^2/muw;
 end
end