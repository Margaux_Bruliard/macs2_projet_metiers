%-------------------------------------------------------------------------------------
% Simulation 2D horizontale d'un écoulement diphasique eau-huile incompressible
% et immiscible.
% Exemple d'un reservoir rectangulaire homogène saturé initalement d'huile.
% On injecte de l'eau par son extrémité gauche. 
% Les conditions aux limites sont de type pression imposée sur ses deux bords.
%
% Les équations sont discrétisées par un schéma volumes finis en espace et un 
% schéma IMPES en temps.
% L'équation en pression est résolue implicitement  et l'équation en saturation 
% est résolue explicitement. la stabilité de ce schéma est assurée en respectant 
% la condition CFL sur le pas de temps.
%-------------------------------------------------------------------------------------
clear all 
close all

% dimensions du domaine [m]
Lx = 1000;
Ly = 1000;
% temps final [s]
tf = 3600*24*360*50

% pression au bord gauche  [Pa]
P_out = 9.E+6;

% pression  au bord droit  [Pa]
P_in = 1.25E+7;

% viscosite de l'huile [Pa/s]
muo=5E-3;

% viscosite de l'eau [Pa/s]
muw = 1.E-3;

% saturation residuelle en huile [-]
Sor = 0.3;

% saturation irreductible en eau [-]
Swi = 0.1;

% permeabilite absolue [m2]
K=1E-13;

% porosite [-]
phi = 0.1;


% saturation initiale
S_init = 1 - Swi;


%Tfinal = 3000
% discretisation en espace : maillage cartesien uniforme
Nx = 18;
Ny = 18;
N = Nx*Ny;
dx = Lx/Nx;
dy = Ly/Ny;
Nint = Ny*(Nx-1) + Nx*(Ny-1); % nombre d'aretes internes
Nbord = 2*Ny;

Ndtmax=10000;

% maillage cartesien uniforme 2D de pas dx dy 
% mailles m = 1:N
for i=1:Nx
    Xi(i) = (i-1/2)*dx;
    for j=1:Ny  
      Yj(j) = (j-1/2)*dy;

      m = i + (j-1)*Nx;

      volume(m) = dx*dy;
      X(m) = Xi(i);
      Y(m) = Yj(j);
    end
end

% structure de donnees aretes interieures: isig = 1:Nint
isig = 0;
% aretes internes verticales 
for i=1:Nx-1
    for j=1:Ny
      m1 = i+(j-1)*Nx;
      m2 = i+1+(j-1)*Nx;
      isig = isig + 1;
      mailleint(isig,1) = m1;
      mailleint(isig,2) = m2;
      surfaceint(isig) = dy;  
      Xint(isig) = (X(m1)+X(m2))/2;
      Yint(isig) = Y(m1);
    end
end 
% aretes internes horizontales
for i=1:Nx
    for j=1:Ny-1
      m1 = i+(j-1)*Nx;
      m2 = i+ j*Nx;
      isig = isig + 1;
      mailleint(isig,1) = m1;
      mailleint(isig,2) = m2;
      surfaceint(isig) = dx;
      Xint(isig) = X(m1);
      Yint(isig) = (Y(m1)+Y(m2))/2;
    end
end

% aretes de bord
isig = 0;
% bord gauche
i=1;
for j=1:Ny
  m1 = i+(j-1)*Nx;
  isig = isig + 1;
  maillebord(isig) = m1;
  surfacebord(isig) = dy;  
  Xbord(isig) = 0;
  Ybord(isig) = Y(m1);
end 
% bord droit
i=Nx;
for j=1:Ny
  m1 = i+(j-1)*Nx;
  isig = isig + 1;
  maillebord(isig) = m1;
  surfacebord(isig) = dy;  
  Xbord(isig) = Lx;
  Ybord(isig) = Y(m1);
end

% initialisation de la permeabilite dans les mailles
for m=1:N
    perm(m) = K;
end

% transmissivites des aretes interieures 
for isig=1:Nint
  m1 = mailleint(isig,1);
  m2 = mailleint(isig,2);
  
  d1_sigma = sqrt((X(m1)-Xint(isig))^2 + (Y(m1)-Yint(isig))^2);
  d2_sigma = sqrt((X(m2)-Xint(isig))^2 + (Y(m2)-Yint(isig))^2);

  % calcul des transmissivites par moyenne harmonique
  Tint(isig) = perm(m1)*perm(m2) * surfaceint(isig) ...
      / (d2_sigma*perm(m1) + d1_sigma*perm(m2)) ; 
end

for isig=1:Nbord
  % transmissivites des aretes de bord
  m = maillebord(isig);
  d_sigma = sqrt((X(m)-Xbord(isig))^2 + (Y(m)-Ybord(isig))^2);
  Tbord(isig) = perm(m) * surfaceint(isig) / d_sigma ; 
end

% Calcul de la pression sur les bords %
Pbord=ones(Nbord,1);

% on construit Pbord
Pbord(1:Nbord/2)=P_in;
Pbord(Nbord/2+1:Nbord)=P_out;


%% SATURATION INTIALE

S_old = S_init*(ones(N, 1));
S_sigma_in = 1.;
S_sigma_out = 0.;
Deltat = 1e7;



%% CALCUL DE LA PRESSION INITIALE:

% calcul de la mobilité Mtint mobilite interne Mtbord mobilite au bord au temps initial %
for isig = 1:Nint
    Mtint(isig) = Mw(1- S_init, Swi, muw) + Mo(S_init, Sor, muo);
end

Mtbord(1:Nbord/2) = Mw(1.0 , Swi, muw); % arrete de bord gauche (on injecte que de l'eau)
Mtbord(Nbord/2+1:Nbord) = Mw(1- S_init, Swi, muw) + Mo(S_init, Sor, muo);  % arrete de bord droit (ce qui est dans le reservoir sort)


%% CALCUL 

temps = 0;

while temps <= tf
    
    
    %% CALCUL DE LA PRESSION
    % isig_=1:Nint;
    % neighbors_=mailleint(isig,:);
    A=sparse(N,N);
    B=sparse(N,1);
    
    % quand on est dans les arretes interieures
    for isig=1:Nint
        m1=mailleint(isig,1);
        m2=mailleint(isig,2);

        coeff=Mtint(isig)*Tint(isig);

        A(m1,m1)=A(m1,m1)+coeff;
        A(m1,m2)=A(m1,m2)-coeff;
        A(m2,m1)=A(m2,m1)-coeff;
        A(m2,m2)=A(m2,m2)+coeff;
    end

    % cas des arretes exterieures
    for isig=1:Nbord
        m=maillebord(isig);

        coeff=Mtbord(isig)*Tbord(isig);

        A(m,m)=A(m,m)+coeff;
        B(m)=B(m)+coeff*Pbord(isig);
    end

    P=A\B;

   
        
    % AFFICHAGE DE LA PRESSION
%     gridX = reshape(X, Ny, Nx);
%     gridY = reshape(Y, Ny, Nx);
%     gridP = reshape(P, Ny, Nx);
%     surf(gridX, gridY, gridP);
%     plot (P);
%     title(sprintf('Pression \nt=%d', temps));
%     pause(0.1);
    
    
    
    %% CALCUL DES FLUX


    % Construction du systeme:

    % Partie des aretes internes
    for isig=1:Nint
        m1 = mailleint(isig,1);
        m2 = mailleint(isig,2);

        Ftint(isig) = Mtint(isig)*Tint(isig)*( P(m1) - P(m2));
    end
    
    
    
    % Partie des aretes de bord
    Ftbord = zeros(1, Nbord);
    for isig = 1:Nbord
        m=maillebord(isig);
        
        Ftbord(isig) = Mtbord(isig)*Tbord(isig)*(P(m) - Pbord(isig));
    end
    
    %% CALCUL DE LA CFL
    
    % on réactualise le pas de temps  au fur et a mesure
   
    % on est dans un maillage régulier, min |K| = volume(m) pour tout m
%     supFlux = sum(min (Ftint, 0)) + sum(min(Ftbord, 0));
%     supFlux = sup( - supFlux);
% 
%     supfoprime = Fop(muo,muw,Sor,Swi,S_old);
%     
%     
%     Deltat = phi*volume(mailleint(1,1)) / (supFlux* ;
    
    
    % Calcul explicite de la saturation:
    % on a besoin de deux tableaux Sn+1 et Sn 
    
    
    
    %% CALCUL DE LA SATURATION
    Saturation = S_old; 
    %calcul du terme sum_int f(Sk)*(Ftint)
    for isig=1:Nint
        m1 = mailleint(isig, 1);
        m2 = mailleint(isig, 2);
        

        terme1 = Fo( muo, muw, Sor, Swi, S_old(m1)) * max( Ftint(isig), 0);
        terme2 = Fo(muo, muw, Sor, Swi, S_old(m2))* min(Ftint(isig), 0);
        
        Saturation(m1) = Saturation(m1) - Deltat/(volume(m1)*phi)*(terme1 + terme2);        
        Saturation(m2) = Saturation(m2) + Deltat/(volume(m2)*phi)*(terme1 + terme2);
    end
    
    
    % saturation au bord gauche: Ssigma = 0;
    for isig=1:Nbord/2
        m = maillebord(isig);
        
        terme = Fo( muo, muw, Sor, Swi, S_old(m)) * max(Ftbord(isig), 0) + ...
                Fo( muo, muw, Sor, Swi, 0) * min( Ftbord(isig), 0);        
        Saturation(m) = Saturation(m) - Deltat/(volume(m)*phi)* terme; 
    end
    
    %saturation au bord droit: Ssigma = 1;
    for isig=Nbord/2+1:Nbord
        m = maillebord(isig);
        
        terme = Fo( muo, muw, Sor, Swi, S_old(m)) * max(Ftbord(isig), 0) + ...
                Fo( muo, muw, Sor, Swi, 1) * min( Ftbord(isig), 0);        
       Saturation(m) = Saturation(m) - Deltat/(volume(m)*phi)* terme;
    end
   
    
    % on incrémente de 1e tableau de la saturation
    S_old = Saturation;
    
    % AFFICHAGE DE LA SATURATION
%     plot(Saturation);
    gridX = reshape(X, Ny, Nx);
    gridY = reshape(Y, Ny, Nx);
    gridS = reshape(Saturation, Ny, Nx);
    surf(gridX, gridY, gridS);
    zlim([0, S_init]);
    title(sprintf('Saturation \nt=%d', temps));
    %axis([0, Nx, 0, Ny, 0, S_init])
    %axis([0, Nx, 0, Ny])
    pause(0.01);
    
    
    
    %% UPDATE MOBILITE Mtint et Mtbord
    for isig=1:Nint
        m1 = mailleint(isig, 1);
        m2 = mailleint(isig, 2);

        if (P(m1) >= P(m2))
            Mtint(isig) = Mw(1- Saturation(m1), Swi, muw) + Mo(Saturation(m1), Sor, muo);
        else
            Mtint(isig) = Mw(1- Saturation(m2), Swi, muw) + Mo(Saturation(m2), Sor, muo);
        end
    end

    for isig=1:Nbord/2
        m = maillebord(isig);

        Mtbord(isig) = Mw (1.0, Swi, muw); 
    end

    for isig=Nbord/2+1:Nbord
        m = maillebord(isig);

        Mtbord(isig) = Mw(1- Saturation(m), Swi, muw) + Mo(Saturation(m), Sor, muo);
    end
   
    
%     disp(sprintf('Mtint au temps %d', temps));
%     disp(Mtint);
    % incrémentation de la boucle 'while'
    temps= temps + Deltat;
    
    
end










