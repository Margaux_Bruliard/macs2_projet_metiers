

function moboil = Mo(So,Sor,muo)
    % Calcul de la mobilité de la phase huile : 
    %
    %           Mo = kro(So)/muo
    %
    % où Kro(So) est la permeabilité relative de l'huile : 
    %
    %           kro(So) = (so - sor)^2 / (1-sor)^2
    %
    % Donnees d'entrees
    % So    : saturation de la phase huile [-]
    % Sor   : saturation résiduelle de la phase huile [-]
    % muo   : viscosité de la phase huile [Pa/s]
    %
    % Donnees de sortie :
    % moboil    : mobilité de l'huile  [s/Pa]
    %
  if (So<Sor)
   moboil = 0;
  else 
   moboil = (So-Sor)^2/(1-Sor)^2/muo;
  end
end