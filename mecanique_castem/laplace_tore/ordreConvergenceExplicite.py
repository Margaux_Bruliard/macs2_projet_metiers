"""
BRULIARD Margaux - RIGAL Mathieu
MACS 2 - Mécanique introduction à CAST3M


Equation de Beltrami-Laplace sur un tore

Etude de la convergence et l'ordre  des erreurs L2 et H1
dans le cas d'un maillage explicite sur un tore
"""

import numpy as np
import matplotlib.pyplot as plt


########################### FICHIERS DE DONNÉES #########################

"""
Données récupérées
"""
tri3_l2 = [0.22568, 0.10215, 2.96631e-2, 7.70986e-3, 1.94658e-3]
tri3_h1 = [0.55376, 0.19658, 5.57201e-2, 1.44436e-2, 3.66380e-3]

qua4_l2 = [0.15869, 6.12211e-2, 1.69724e-2, 4.35280e-3, 1.09514e-3]
qua4_h1 = [0.29138, 0.10257, 2.77114e-2, 7.06073e-3, 1.77354e-3]

tri6_l2 = [3.68482e-2, 2.89908e-3, 2.41452e-4, 2.47463e-5, 3.79529e-6]
tri6_h1 = [0.12035, 1.85676e-2, 4.08161e-3, 9.81242e-4, 2.43040e-4]

qua8_l2 = [0.10528, 2.55472e-2, 6.25706e-3, 1.55553e-3, 3.89228e-4]
qua8_h1 = [0.19473, 4.36609e-2, 1.03591e-2, 2.55156e-3, 6.37379e-4]


#on pose dans @xvalues le nombre de mailles: un maillage 5x10 a donc 50 mailles
xvalues = [50, 200, 800, 3200, 80*160]


## GRAPHIQUES D'ERREURS
#plt.plot(xvalues, tri3_l2, label="'erreur L2 pour les éléments COQ3")
#plt.plot(xvalues, tri3_h1, label="erreur H1 pour les elements COQ3")
#plt.legend();
#plt.title("Courbes d'erreurs pour les éléments COQ3")
#plt.xlabel("nombres de mailles")
#plt.ylabel("erreur")
#plt.show();
#
#
#plt.plot(xvalues, qua4_l2, label="'erreur L2 pour les éléments COQ4")
#plt.plot(xvalues, qua4_h1, label="erreur H1 pour les elements COQ4")
#plt.legend();
#plt.title("Courbes d'erreurs pour les éléments COQ4")
#plt.xlabel("nombres de mailles")
#plt.ylabel("erreur")
#plt.show();
#
#
#plt.plot(xvalues, tri6_l2, label="'erreur L2 pour les éléments COQ6")
#plt.plot(xvalues, tri6_h1, label="erreur H1 pour les elements COQ6")
#plt.legend();
#plt.title("Courbes d'erreurs pour les éléments COQ6")
#plt.xlabel("nombres de mailles")
#plt.ylabel("erreur")
#plt.show();
#
#
#plt.plot(xvalues, qua8_l2, label="'erreur L2 pour les éléments COQ8")
#plt.plot(xvalues, qua8_h1, label="erreur H1 pour les elements COQ8")
#plt.legend();
#plt.title("Courbes d'erreurs pour les éléments COQ8")
#plt.xlabel("nombres de mailles")
#plt.ylabel("erreur")
#plt.show();


## Tracer des courbes d'erreurs 
ordre1 = []
ordre2 = []
ordre1_2 = []
for i in range(len(xvalues)):
    ordre1.append(1/xvalues[i])
    ordre2.append(1/(xvalues[i]**2))
    ordre1_2.append(1/(xvalues[i]**1.5))


##### COQ 3 #####
#plt.loglog()
#plt.plot(xvalues, tri3_l2, label="erreur L2", color='c')
#plt.plot(xvalues, ordre1, label="ordre 1", color='y')
#plt.plot(xvalues, ordre2, label='ordre 2', color='g')
##plt.plot(xvalues, ordre3, label='ordre 3', color='b')
#plt.plot(xvalues, tri3_h1, label="erreur H1", color='r')
#plt.legend(loc='best');
#plt.title("Courbes d'erreurs pour les éléments COQ3")
#plt.xlabel("nombres de mailles")
#plt.ylabel("erreur")
#plt.show();

##### COQ 4 #####
plt.loglog()
plt.plot(xvalues, qua4_l2, label="erreur L2", color='c')
plt.plot(xvalues, ordre1, label="ordre 1", color='y')
plt.plot(xvalues, ordre2, label='ordre 2', color='g')
#plt.plot(xvalues, ordre3, label='ordre 3', color='b')
plt.plot(xvalues, qua4_h1, label="erreur H1", color='r')
plt.legend(loc='best');
plt.title("Courbes d'erreurs pour les éléments COQ4")
plt.xlabel("nombres de mailles")
plt.ylabel("erreur")
plt.show();

##### COQ 6 #####
plt.loglog()
plt.plot(xvalues, tri6_l2, label="erreur L2", color='c')
plt.plot(xvalues, tri6_h1, label="erreur H1", color='r')

#plt.plot(xvalues, ordre1, label="ordre 1", color='y')
plt.plot(xvalues, ordre2, label='ordre 2', color='g')
plt.plot(xvalues, ordre1_2, label='ordre 1.5', color='b')

plt.legend(loc='best');
plt.title("Courbes d'erreurs pour les éléments COQ6")
plt.xlabel("nombres de mailles")
plt.ylabel("erreur")
plt.show();



##### COQ 8 #####
plt.loglog()
plt.plot(xvalues, qua8_l2, label="erreur L2", color='c')
plt.plot(xvalues, ordre1, label="ordre 1", color='y')
plt.plot(xvalues, ordre2, label='ordre 2', color='g')
#plt.plot(xvalues, ordre3, label='ordre 3', color='b')
plt.plot(xvalues, qua8_h1, label="erreur H1", color='r')
plt.legend(loc='best');
plt.title("Courbes d'erreurs pour les éléments COQ8")
plt.xlabel("nombres de mailles")
plt.ylabel("erreur")
plt.show();