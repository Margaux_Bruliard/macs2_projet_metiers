************************************************************************
* NOM         : TP1
* DESCRIPTION : Un Laplacien avec une solution analytique en
*               \prod cos(PI x_i)   
*
*
* LANGAGE     : GIBIANE-CAST3M
* AUTEUR      : St�phane GOUNAND (CEA/DEN/DM2S/SEMT/LTA)
*               m�l : stephane.gounand@cea.fr
**********************************************************************
* VERSION    : v1, 02/05/2017, version initiale
* HISTORIQUE : v1, 02/05/2017, cr�ation
* HISTORIQUE :
* HISTORIQUE :
************************************************************************
*
* Options globales
*
'OPTI' 'DIME' 2 ;
'OPTI' 'ELEM' 'QUA4' ;
*
* Maillage
*
* nx : nombre de maille par c�t�
nx = 4 ;
* Points
pA = 0. 0. ;
pB = 1. 0. ;
pC = 1. 1. ;
pD = 0. 1. ;
* Lignes
lAB = 'DROI' nx pA pB ;
lBC = 'DROI' nx pB pC ;
lCD = 'DROI' nx pC pD ;
lDA = 'DROI' nx pD pA ;
* Surface
mt = 'DALL' lAB lBC lCD lDA ;
*mt = 'SURF' (lAB 'ET' lBC 'ET' lCD 'ET' lDA) ;
*
'TRAC' mt 'TITR' 'MAILLAGE' ;
*
* Solution analytique 
*
* Attention, les angles sont en degr�s dans Castem !
xmt ymt = 'COOR' mt ;
pcos = ('COS' (xmt '*' 180.)) '*' ('COS' (ymt '*' 180.)) ;
solana = 'NOMC' 'T' pcos ;
'TRAC' solana mt 'TITR' 'Solution analytique' ;
evsola = 'EVOL' 'CHPO' solana lAB ;
'DESS' evsola 'TITR' 'Solution analytique sur lAB' ;
*
* Solution num�rique
*
modt = 'MODE' mt 'THERMIQUE' ;
matt = 'MATE' modt 'K' 1. 'RHO' 1. 'C' 1. ;
* matrice du laplacien * -1
mlap = 'COND' modt matt ;
* terme source
tsou = 'SOUR' modt (2. '*' (pi '**' 2) '*' solana) ;
* conditions aux limites
cmt = 'CONT' mt ;
mcli = 'BLOQ' 'T' cmt ;
fcli = 'DEPI' mcli ('REDU' solana cmt) ;
* r�solution
mat = mlap 'ET' mcli ;
smb = tsou 'ET' fcli ;
sol = 'RESO' mat smb ;
*
* Post-traitement : calcul des erreurs en norme L2
*                   et en semi-norme H1 
*
* Trac� de la solution
solnum = 'EXCO' 'T' sol 'T' ;
'TRAC' solnum mt 'TITR' 'Solution numerique' ;
* Trac� du gradient de la solution
gsol = 'GRAD' modt solnum ;
lcomp = 'MOTS' 'T,X' 'T,Y' ;
vgsol = 'VECT' gsol modt ('/' 0.5 nx) lcomp ('MOTS' jaun) ;
'TRAC' vgsol mt 'TITR' 'Gradient de la solution' ;
* Calcul des erreurs L2 et H1
dif = '-' solana solnum ;
'TRAC' dif mt 'TITR' 'Difference analytique-numerique' ;
errinf = 'MAXI' ('ABS' dif) ;
errpl2 = (('XTX' dif) '/' ('NBNO' mt)) '**' 0.5 ;
mmas = 'CAPA' modt matt ;
errl2 = ('XTMX' dif mmas) '**' 0.5 ;
errh1 = ('XTMX' dif mlap) '**' 0.5 ;
'MESS' 'Erreur Linf        =' errinf ;
'MESS' 'Erreur pseudo-L2   =' errpl2 ;
'MESS' 'Erreur L2          =' errl2 ;
'MESS' 'Erreur H1          =' errh1 ;




'OPTION' 'DONN'  5 ;







*
* End of dgibi file TP1
*
'FIN' ;

